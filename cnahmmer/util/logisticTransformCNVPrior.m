function [F,freqThres,logitAlpha] = logisticTransformCNVPrior(freqs,filterLevel,plotImage)
%% freqs is double array containing frequencies (Nx1)
%% filterLevel is a single value in [0 1]

if (sum(freqs>0)==0)
  F = freqs;
else
  F = zeros(length(freqs),1);

% only work with the non-zero frequencies and those that meet threshold
  freqThres = estimateCNVFreqThreshold(freqs,filterLevel);
  freqToUseInd = freqs > 0 & freqs >= freqThres;
  [logitAlpha,jnk] = getLogitAlpha(freqs(freqToUseInd));


  F(freqToUseInd) = logisticTransform(freqs(freqToUseInd),logitAlpha);
end

if plotImage==1
    figure;
    plot(freqs(freqToUseInd),F(freqToUseInd),'.');
    xlim([0 .5]);
    ylim([0 1]);
end
