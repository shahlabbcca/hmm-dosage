function uniprot = hugo2uniprot(hugoFile, assocFile,outfile)
% function uniprot = hugo2uniprot(hugoFile, assocFile)
% converts hugo gene names to uniprot gene names
% hugoFile  - list of hugo gene names
% assocFile - gene ontology association file


[u, uniprotid, spid, junk, goid, ref, g, h, k, genedesc, ipiid, p, taxid, dat, un] = ...
    textread(assocFile, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s', ...
        'headerlines',25,'delimiter','\t');
    
hugo = textread(hugoFile,'%s');
uniprot=hugo;
fid = fopen(outfile,'wt');
for i = 1:length(hugo) 
    I = strmatch(hugo{i},genedesc);
    if isempty(I)
       disp(['WARNING: gene name: ', hugo{i}, ' not found in association file']);
    else
        uniprot{i} = uniprotid{I(1)};
    end
    fprintf(fid,'%s\n',uniprot{i});
end
fclose(fid);


    
    