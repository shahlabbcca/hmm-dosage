function probs = states2Probs(states,S)
    % function that takes a set of sampled state sequences and 
    % converts them to conditional marginals p(S_i=s|\theta,data)
    [numS,N] = size(states);
    probs = zeros(S,N);
    counts = zeros(S,N);
    for t=1:N
        for s=1:S
            counts(s,t)=sum(states(:,t)==s);
        end
    end
    probs = counts/numS;