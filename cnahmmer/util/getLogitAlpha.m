function [alpha,multiplier] = getLogitAlpha(X)

multiplier = 0;
alpha = 0;
while (alpha < 0.1)
    multiplier = multiplier + 1;
    alpha = multiplier*std(X);    
end