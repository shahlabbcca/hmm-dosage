function Z = intersectCalls(Z1,Z2)

Z = ones(size(Z1))*2;
Zl = (Z1==1) & (Z2==1);
Zg = (Z1==3) & (Z2==3);

Z(Zl) = 1;
Z(Zg) = 3;
