function Y = unitNormalise(Y)
% takes in a matrix of values and for each vector ensures 0 mean and unit
% variance
[P,N] = size(Y);
for p = 1:P
    y = Y(p,:);
    muy = nanmean(y);
    stdy = nanstd(y);
    Y(p,:) = (y-muy)/stdy;
end