function probs = freqMatrix2ProbsLNG(matrix)
    [N,numS] = size(matrix);
    probs = zeros(3,N);
    
    losses = matrix==1;
    probs(1,:) = sum(losses,2)/numS;
    neutrals = matrix == 2;
    probs(2,:) = sum(neutrals,2)/numS;
    gains = matrix == 3;
    probs(3,:) = sum(gains,2)/numS;
    
    
    
    