function thres = estimateCNVFreqThreshold(X,level)
%% X = vector of CNV frequencies
%% change = criteria for choosing threshold based on ecdf difference
%% return thres = optimal threshold
%% thres will always be >= 1% cnv frequency

nonZero = X > 0;

% bins = 0:0.0010:1;               
% h = hist(X(nonZero),bins); 
[f,x] = ecdf(X(nonZero));
levelInd = find(f >= level);
thres = x(levelInd(1));




% fdiff = f(2:end) - f(1:end-1);
% fdiffLT = find(fdiff < change);
% minCNVFreq = find(x>=min);
% GT1perc = find(fdiffLT >= minCNVFreq(1));
% thres = x(fdiffLT(GT1perc(1)));