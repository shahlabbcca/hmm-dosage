function Z = medianFilter(Y,chrIndex,N)
[P,n] = size(Y);
Z = Y;
for p=1:P 
    for i = 1:23
        I = chrIndex==i;
        Z(p,I) = medfilt1(Y(p,I),N);
    end
end
