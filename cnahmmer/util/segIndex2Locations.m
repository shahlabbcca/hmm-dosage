function segments = segIndex2Locations(segs,starts,stops)
    numC = length(segs);
    numSegs = 0;
    labels = [-1 0 1];
    for c = 1:numC
        [m n] = size(segs{c});
        numSegs = numSegs+m;
    end
    segments = zeros(numSegs,5);
    index = 1;
    for c =1:numC
        [m n] = size(segs{c});
        for i = 1:m
            segments(index,:) = [c starts{c}(segs{c}(i,1)) stops{c}(segs{c}(i,2)) labels(segs{c}(i,3)) segs{c}(i,4)];
            index=index+1;
        end
        
    end
    