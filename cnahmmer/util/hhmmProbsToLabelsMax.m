function [probs, labels] = hhmmProbsToLabelsMax(pM)
    % FUNCTION [probs, labels] = hhmmProbsToLabels(pM)
    % PARAMS:
    %   pM - matrix representing p(loss,neutral,gain,undefined) for every
    %        probe
    %   pThresh - probability threshold above which we assign a state label
    % RETURN: 
    %   probs(t,k) -   a 3-column matrix probs, where probs(t,1) = p(loss),
    %       probs(t,2) = p(neutral) and p(t,3) = prob(gain)
    %   labels(t)  -   a hard assignment of the probabilities indicating
    %       the presence of a loss (-1), neutral (0) or gain (1)
    probs = pM(1:3,:);
    
    [junk labels] = max(probs,[],1);
    labels(find(labels==1)) = -1;
    labels(find(labels==3)) = 1;
    labels(find(labels==2)) = 0;
    
    
    