function gamma = convertTo3State(gammaIn,K)
    gamma = zeros(3,length(gammaIn));
    if K==3
        gamma=gammaIn;
    end
    
    if K==4
        gamma(1,:) = gammaIn(1,:);
        gamma(2,:) = gammaIn(2,:);
        gamma(3,:) = sum(gammaIn(3:4,:));
    end
    
    if K==5
        gamma(1,:) = sum(gammaIn(1:2,:));
        gamma(2,:) = gammaIn(3,:);
        gamma(3,:) = sum(gammaIn(4:5,:));
    end
    
    if K==6
        gamma(1,:) = sum(gammaIn(1:2,:));
        gamma(2,:) = gammaIn(3,:);
        gamma(3,:) = sum(gammaIn(4:6,:));
    end