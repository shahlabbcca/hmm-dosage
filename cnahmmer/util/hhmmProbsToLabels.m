function [probs, labels] = hhmmProbsToLabels(pM,pThresh)
    % FUNCTION [probs, labels] = hhmmProbsToLabels(pM)
    % PARAMS:
    %   pM - matrix representing p(loss,neutral,gain,undefined) for every
    %        probe
    %   pThresh - probability threshold above which we assign a state label
    % RETURN: 
    %   probs(t,k) -   a 3-column matrix probs, where probs(t,1) = p(loss),
    %       probs(t,2) = p(neutral) and p(t,3) = prob(gain)
    %   labels(t)  -   a hard assignment of the probabilities indicating
    %       the presence of a loss (-1), neutral (0) or gain (1)
    probs = pM(1:3,:);
    
    labels = zeros(1,length(pM));
    loss = find(probs(1,:) > pThresh);
    gain = find(probs(3,:) > pThresh);
    labels(loss) = -1;
    labels(gain) = 1;
    
    
    