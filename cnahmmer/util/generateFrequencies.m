function [pM, piStat] = generateFrequencies(Z,S)
    [numS,N] = size(Z);
    pM = zeros(S,N);
    piStat = zeros(1,S);
    
    for k=1:S
        piStat(k)=sum(sum(Z==k))/(numS*N);
        pM(k,:) = sum(Z==k)/numS;
    end
    
        
    