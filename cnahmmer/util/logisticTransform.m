function Flogistic = logisticTransform(F,alpha)
%% F = vector to be logistic transformed
%% alpha = parameter to scale logistic curve
%% logisticF = 

% standarize CNV frequencies
Fstd = (F-mean(F)) ./ std(F);
% perform logit on CNV frequencies
Flogistic = ones(size(Fstd)) ./ (1+exp(-Fstd/alpha));