function pZ = calls2Probs(Z,K)
    [P,N] = size(Z);
    pZ = zeros(P,K,N);
    for p=1:P
        for k=1:K
            pZ(p,k,find(Z(p,:)==k))=1;
        end
    end
    