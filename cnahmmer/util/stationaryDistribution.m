function pi = stationaryDistribution(A)
% function pi = stationaryDistribution(A)
% compute the stationary distribution pi of a Markov chain from the transition
% matrix A
K = length(A);
    pi = ones(1,K)/(eye(K)-A+ones(K));
    