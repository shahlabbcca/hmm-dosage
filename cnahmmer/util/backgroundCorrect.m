function [X,Y,Yprime,chrIndex,mu,sigma,scalingFactor] = backgroundCorrect(datadirNormal,datadirTumour) 

[mu,sigma] = computeBackground(datadirNormal);

datafile = 'data.dat';
scaffoldfile = 'scaffold.dat';

[X,Y,chrIndex,clones,starts,stops] = importMultiSampleData(fullfile(datadirTumour,scaffoldfile), ...
    fullfile(datadirTumour,datafile),-1);
alpha = 10;
[P,N] = size(Y);
scalingFactor = repmat(mu./(1+alpha*sigma),[P,1]);

Yprime = Y-scalingFactor;
