function [loss gain total] = calculatePercentChange(filename)
% do loss/gain analysis
% count number of loss probes, gain probes, total aberrated probes
% output a tab delimited text file with the following format:
% <bar code> <%loss> <%gain> <%total> <%survival>
%
[clones chrIndex starts stops ploss pneut pgain labels] = ...
        textread(filename,'%s\t%d\t%d\t%d\t%f\t%f\t%f\t%f');
N = length(chrIndex);
loss = sum(labels==-1)/N;
gain = sum(labels==1)/N;
total = loss+gain;


