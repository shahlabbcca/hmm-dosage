% evaluate p(Dirichlet|Multinomial) for given parameterizations
% by alpha for Dirichlet and mu for Multinomial
% see Bishop, PatternRecognition and Machine Learning p76-77
% constraints: alpha_i > 0, 0 <= mu_i <= 1, sum(mu_i)==1
function logp = dirichletPrior(alpha,mu)
        alpha=alpha+eps; % satisfies constraint that alpha_i > 0
        mu=mu+eps;
        logp = log(gamma(sum(alpha))) - sum(log(gamma(alpha))) + sum(log(mu.^(alpha-1)));

