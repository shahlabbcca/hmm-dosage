function [mu_N, sigma_N, a_N, b_N] = updateNoiseHyperparamsWeightedSS(Y,pZ, ...
    mu_0,var_0,a_0,b_0,sigmas)
    % function updateNoiseHyperparams
    % ---------------------------------------------------------------------
    % this function uses the updating equations for a Normal-Gamma
    % distribution as described by Bishop, Pattern Recognition and 
    % Machine Learning, pgs 97-102
    % Parameterization is 
    [S,n] = size(pZ);   
    mu_N     = mu_0;
    sigma_N  = sqrt(var_0);
    a_N  = a_0;
    b_N  = b_0;
    
    for k=1:S
        % only update if there is data for this class, otherwise
        % default to the prior
        N = sum(pZ(k,:));
        if N>0
            % use conjugate Gaussian prior - assume variance is known
            % (from last iteration (given as sigmas)

            [mu_ML, N] = weightedMean(pZ(k,:),Y);
            var_ML = weightedVariance(pZ(k,:),Y,mu_ML);

            % assume the variance is known
            v = sigmas(k)^2; % previous estimate
            v_0 = var_0(k);  % variance of prior mean

            p = 1/(N*v_0 + v);

            % update Gaussian mean hyperparams
            mu_N(k) = p*(v*mu_0(k) + N*v_0*mu_ML);    
            prec_N = 1/v_0 + N/v;
            sigma_N(k) = 1/sqrt(prec_N);


            % update Gaussian variance hyperparams
            a_N(k) = a_0(k) + N/2;
            b_N(k) = b_0(k)+(N*var_ML)/2;

        end
    end

