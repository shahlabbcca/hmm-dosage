function py = masterLocalEvidenceNolink(Az,Al,Z)
    % masterObservationModel(A,L,Z)
    % 
    % FUNCTION
    % Calculate the local evidence to be used in fwds-bwds
    % to infer the master state sequence
    %
    % PARAMS
    % Az  : transition matrix for free slaves (S by S)
    % Al  : transition matrix for yoked slaves (S by S)
    % Z   : state sequence for slaves (numS by N \in {1:S})
    % where numS is the number of slaves
    %
    % RETURN
    % py : S+1 by N matrix containing the local evidence
    
    % constants for 'yoking'
    
    [numS N] = size(Z); % number of slaves, length of the sequence
    S = length(Az);     % number of states the Sth state is don't care
     
    py = zeros(S+1,N);
    t = 1;
    py(:,1)=[0.1 0.3 0.1 0.5];
    epsilon = Al(1,1);

    for t = 2:N
        c = zeros(1,S);
        for k=1:S
            c(k) = sum(Z(:,t)==k);                        
            %XL = [Z(:,t),ones(numS,1)*k];
            %I = subv2ind(size(Al),XL);
            eprime = (1-epsilon)/(S-1);
            py(k,t) = (epsilon^c(k))*(eprime^(numS-c(k))); 
          %prod(Al(I));            
   %         py(k,t) = sum(log(Al(I)+1));
        %    py(k,t) = logsumexp(Al(I));            
        end
        frac = max(c)/numS;
        X = [Z(:,t-1),Z(:,t)];    
        I = subv2ind(size(Az),X);
        %py(k+1,t) = (S*(1-epsilon))*prod(Az(I));
        py(k+1,t) = prod(Az(I));
        %py(k+1,t) = sum(log(Az(I)+1));
        %py(k+1,t) = logsumexp(Az(I));

        py(:,t) = normalise(py(:,t));
    end
   %py = py + 1e-8;
%   figure(25); clf; plot(log(py'));
    
    
    
    
    
    
    
    
    
