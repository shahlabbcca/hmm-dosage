function [z] = truncGaussSample(mu, sigma2, a, b, n)
% truncGaussSample Sample from Gaussian truncated between a and b
% Based on TruncatedGaussRND from http://mahieddine.ichir.free.fr/Matlab/matlab.html
if nargin < 5, n = 1; end
x0 = mu;
lambda = 1/(2*sigma2);
u = rand(n,1);
y1 = sqrt(lambda)*(a - x0);
y2 = sqrt(lambda)*(b - x0);
z = (erfinv(u*erf(y2) + (1-u)*erf(y1)))/sqrt(lambda) + x0;
