function [V,N] = weightedVariance(pZ,Y,mu)
    N = sum(pZ);
    V = nansum(pZ.*(Y-mu).^(ones(1,length(Y))*2))/N;
    