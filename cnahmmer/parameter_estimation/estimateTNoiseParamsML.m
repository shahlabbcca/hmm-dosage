function [mu_N, lambda_N, pi, u] = estimateTNoiseParamsML(y,mu,lambda,nu,rho)
    % give the previous estimate of the parameters and the current 
    % estimate of the reponsibilities, estimate the new parameters
    % function [mu_N, lambda_N, pi, u] =
    % estimateTNoiseParamsML(y,mu,lambda,nu,rho)
    %
    % y      - data
    % mu     - previous estimate of the Student-t means
    % lambda - previous estimate of the Student-t precision
    % nu     - fixed dof
    % rho    - current estimate of the responsibilites
    %
    % RETURN:
    % =======
    % mu_N     - new ML estimate of means
    % lambda_N - new ML estimate of precisions
    % pi       - new ML estimate of stationary distribution
    % u        - estimate of 'hidden' scale parameter u
    
    [M,K] = size(mu);
    [W,N] = size(y);
    
    yr = repmat(y,[K,1]);
    mur = repmat(mu',[1,N]);
    lambdar = repmat(lambda',[1,N]);
    nur = repmat(nu',[1,N]);
    
    o = ones([K,N]);
    
    num = o + nur;
    den = ((yr-mur).^2).*lambdar + nur;
    
    u = num./den;
    
    mu_N = sum(rho.*u.*yr,2)./sum(rho.*u,2);
    mu_Nr = repmat(mu_N,[1,N]);
    lambda_N = sum(rho,2)./sum(rho.*u.*((yr-mu_Nr).^2),2);
    pi = mean(rho,2);
    
    
    