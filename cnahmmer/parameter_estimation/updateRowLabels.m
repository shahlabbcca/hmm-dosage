function prob = updateRowLabels(rowLabels,columnLabels,data,patternPrior,backgroundPrior)
    numRows = length(rowLabels);
    numCols = length(columnLabels);
    prob = zeros(numRows,2);
    for i = 1:numRows
        