function counts = getCountsCMM(Z,S)
    [numS,N] = size(Z);
    counts = zeros(S,S,N);
    counts(:,:,1) = ones(S);
    for s = 1:numS
        for t = 2:N
            counts(Z(s,t-1),Z(s,t),t) = counts(Z(s,t-1),Z(s,t),t) + 1;
        end
    end
    