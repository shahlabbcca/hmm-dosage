function w = dirichletMixturePDF(observedCounts,priors,priorW)
    [P,K,N] = size(priors);
    w = zeros(P,N);
  
    for p = 1:P
        pr = reshape(priors(p,:,:),[K,N]);
        w(p,:) = dirichletpdfV(observedCounts,pr);
    end
    
    w = normalise(w.*repmat(priorW,[1,N]),1);
%     
%         for n = 1:N
%             pr = reshape(priors(p,:,n),[K,1]);
%             sumPr = sum(pr);
%             c = weights(p,n)*gamma(Nc+1)*sumPr/gamma(Nc+sumPr);
%             w(p,n) = c*prod(gamma(counts(:,n)+pr)./(gamma(counts(:,n)+1).*gamma(pr)));
%             %w(p,n) = weights(p,n)*dirichletpdf(reshape(priors(p,:,n),[K,1]),observedCounts(:,n)+1);
%         end
%     end
%     w = normalise(w,1);