function y=invChiSquaredPdf(x,nu,sigma,mu)
    % FUNCTION: invChiSquaredPdf(x,nu,sigma)
    %           computes the symmetric pdf of on inverse Chi Squared
    %           distribution with dof=nu and shape=sigma.  The valley of
    %           the curve is centered at mu.
    y=zeros(size(x));
    Ineg=find(x<=mu);
    Ipos=find(x>mu);
    y(Ineg)=chi2invpdf(mu-x(Ineg),nu,sigma);
    y(Ipos)=chi2invpdf(x(Ipos)-mu,nu,sigma);
    
    
    y=0.5*y;