function [mus_hat, sigmas_hat] = sampleGaussianNoiseParams(mu_N,sigma_N,a_N,b_N)
    % function sampleGaussianNoiseParams
    % ---------------------------------------------------------------------
    % this function uses the updating equations for a Normal-Gamma
    % distribution as described by Bishop, Pattern Recognition and 
    % Machine Learning, pgs 97-102
    % Parameterization is     mu ~ Gaussian(mu_N,sigma_N)
    %                      sigma ~ sqrt(1/Gamma(a_N,b_N))
    
    % now sample from the gaussian to get the mean
    mus_hat = normrnd(mu_N,sigma_N);

    % sample from the gamma and calculate the std dev
    lambda = gamrnd(a_N,ones(size(mu_N))./b_N);
    sigmas_hat = sqrt(ones(size(mu_N))./lambda);