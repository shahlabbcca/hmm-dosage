function A = sampleDirichletMat(Z,prior)
    % sample from a Dirichlet given observed states and the pseudocounts
    [S S] = size(prior);
    A = zeros(S);
    % get the number of samples
    [numS, N] = size(Z);

    % if there is more than one sample, iterate over them and update the 
    % counts accordingly
    counts = prior;
    for s = 1:numS        
        counts = counts + getCounts(Z(s,:),S);
    end
    for s = 1:S
        A(s,:) = DirichletRND(counts(s,:));
    end
    