function py = GaussRobustObs(y,sizes,mus,sigmas)
% Compute the observation density for a DBN 
% 
% Params:
% y        - data
% sizes    - dimensions of parents of y (discrete). Ensure the states are the
%            last component of this vector
% mus      - means of states (must be of size [1 sizes(end)])
% sigmas   - sigmas of states (must be of size [1 sizes(end)])
% epsilons - probability of outlier within the 'hole' of the distribution
%mus
%sigmas
o = 1; z = 2;
inlier = 1; outlier = 2;
inds = ind2subv(sizes,1:prod(sizes));
py=zeros(prod(sizes),length(y));
globalSigma=nanstd(y);

Inan = find(isnan(y));
In   = find(~isnan(y));



outI = find(inds(:,o)==outlier);
for i=1:length(outI)
    %py(outI(i),:)=outsideRange(y,mus(i),sigmas(i),epsilons(i));
    py(outI(i),In)=holeyDensity(y(In),mus(i),globalSigma)+1e-8;
    py(outI(i),Inan)=1;
end


inI = find(inds(:,o)==inlier);
for i=1:length(inI)
    %py(inI(i),:)=truncNormPdf(y,mus(i),sigmas(i),lower(i),upper(i));
    py(inI(i),In)=normpdf(y(In),mus(i),sigmas(i))+1e-8;
    py(outI(i),Inan)=1;
end


%py = normalise(py,1);
%py(:,1:20)