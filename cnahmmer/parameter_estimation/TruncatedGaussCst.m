function [K] = TruncatedGaussCst(lambda,x0,a,b)
y1 = sqrt(lambda)*(a - x0);
y2 = sqrt(lambda)*(b - x0);
K = sqrt(pi/lambda)*(erf(y2) - erf(y1))/2;