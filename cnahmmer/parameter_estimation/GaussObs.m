function py = GaussObs(y,mus,sigmas)
% Compute the observation density 
% 
% Params:
% y        - data
% mus      - means of states (must be of size [1 sizes(end)])
% sigmas   - sigmas of states (must be of size [1 sizes(end)])

py=zeros(length(mus),length(y));
nanI = find(isnan(y));
nI   = find(~isnan(y));
for i=1:length(mus)
    py(i,nI)=normpdf(y(nI),mus(i),sigmas(i));
    py(i,nanI)=ones(1,length(nanI));
end
%py(find(isnan(py)))=0;
%py=normalize(py,2);
py=py+1e-8;