function [mu_N, lambda_N, pi, u] = estimateTNoiseParamsMap(y,mu,lambda,nu,rho,eta,m,gamma,S,kappa)
    % give the previous estimate of the parameters and the current 
    % estimate of the reponsibilities, estimate the new parameters
    % Author: Sohrab Shah <sshah@cs.ubc.ca>
    % function [mu_N, lambda_N, pi, u] =
    % estimateTNoiseParamsML(y,mu,lambda,nu,rho)
    %
    % y      - data
    % mu     - previous estimate of the Student-t means
    % lambda - previous estimate of the Student-t precision
    % nu     - fixed dof
    % rho    - current estimate of the responsibilites
    % eta    - prior precision on mu
    % m      - prior mean on mu
    % gamma  - prior shape on lambda (Gamma(shape,scale))
    % S      - prior scale on lambda (Gamma(shape,scale))
    %    
    % RETURN:
    % =======
    % mu_N     - new ML estimate of means
    % lambda_N - new ML estimate of precisions
    % pi       - new ML estimate of stationary distribution
    % u        - estimate of 'hidden' scale parameter u
    % 
    % Based on:
    % 
    
    [M,K] = size(mu);
    [W,N] = size(y);
    
    % vectorize all parameters
    yr      = repmat(y,[K,1]);
    mur     = repmat(mu',[1,N]);
    lambdar = repmat(lambda',[1,N]);
    nur     = repmat(nu',[1,N]);
     
 
    % calculate the hidden scale parameter
    o = ones([K,N]);  
    num = o + nur;
    den = ((yr-mur).^2).*lambdar + nur;   
    u = num./den;
    
  % calculate the mean
    mu_N = (nansum(rho.*u.*yr,2) + (eta.*m)')./(nansum(rho.*u,2) + eta');
    mu_Nr = repmat(mu_N,[1,N]);
    
    % calculate the precision
    lambda_N = (nansum(rho,2) + gamma' +1)./(nansum(rho.*u.*((yr-mu_Nr).^2),2) + (eta'.*(mu_N-m').^2 + S'));
     
    % calculate the stationary distribution
    pi = (nansum(rho,2)+kappa'-1)./(N + nansum(kappa) + K);
    
    
    
    