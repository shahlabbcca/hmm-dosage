function [mus_hat, sigmas_hat] = sampleGaussianNoiseParamsTrunc(mu_N,sigma_N,a_N,b_N,lBound,uBound,sBound)
    % function sampleGaussianNoiseParamsTrunc
    % ---------------------------------------------------------------------
    % samples means and std-devs of a Gaussian-Gamma distribution given
    % truncated priors 
    % mu ~ Gauss(mu_N,sigma_N)*I(lBound <= mu <= uBound)
    % sigma ~ Gamma(a_N,b_N)*I(sigma < sBound)
    
    [numS,S] = size(mu_N);
    sigmas_hat = zeros(numS,S);
    mus_hat = zeros(numS,S);
    
    for s=1:numS
        for k=1:S
            mus_hat(s,k)    = ...
                samplingFromTwoSideTruncatedNormal(mu_N(s,k),sigma_N(s,k), ...
                    lBound(s,k),uBound(s,k));
            sigmas_hat(s,k) = ...
                sampleTruncatedSigma(a_N(s,k),b_N(s,k),sBound(s,k));
        end
    end
    