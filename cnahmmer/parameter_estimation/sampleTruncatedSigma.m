function x = sampleTruncatedSigma(a, b, bound)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input:
%   a is the shape parameter of the inverse gamma distribution.
%   b is the scale parameter of the inverse gamma distribution.
%   right is the up bound of x.
%
% Output:
%   x is the random number following truncated inverse gamma, 
%   and x < right.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This method is based on book "Non-Uniform Random Variate Generation"
% by Devroye (1986) 

y = samplingFromOneSideTruncatedGamma(a, 1/b, 1/bound^2);
x = sqrt(1/y);
