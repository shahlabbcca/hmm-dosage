function p = tdistPDF(x,mu,lambda,nu)
    a = (gamma(nu/2+0.5)/gamma(nu/2))*((lambda/(pi*nu))^(0.5));
    a = repmat(a,size(x));
    lambda = repmat(lambda,size(x));
    mu = repmat(mu,size(x));
    nu = repmat(nu, size(x));
    
    b = (ones(size(x))+(lambda.*(x-mu).^2)./nu).^(-(0.5).*nu-0.5);
    p = a.*b;
    p(find(isnan(p)))=1;