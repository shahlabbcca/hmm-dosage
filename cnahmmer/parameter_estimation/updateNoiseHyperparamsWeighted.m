function [mu_N, sigma_N, a_N, b_N] = updateNoiseHyperparamsWeighted(Y,pZ, ...
    mu_0,var_0,a_0,b_0,sigmas)
    % function updateNoiseHyperparams
    % ---------------------------------------------------------------------
    % this function uses the updating equations for a Normal-Gamma
    % distribution as described by Bishop, Pattern Recognition and 
    % Machine Learning, pgs 97-102
    % Parameterization is 
    [numS,S,n] = size(pZ);   
    mu_N     = mu_0;
    sigma_N  = sqrt(var_0);
    a_N  = a_0;
    b_N  = b_0;
    
    for s=1:numS
        pZs = reshape(pZ(s,:,:),S,n);
        for k=1:S
            % only update if there is data for this class, otherwise
            % default to the prior              
            N = sum(pZs(k,:));
            if N>0
                % use conjugate Gaussian prior - assume variance is known
                % (from last iteration (given as sigmas)

                [mu_ML, N] = weightedMean(pZs(k,:),Y(s,:));
                var_ML = weightedVariance(pZs(k,:),Y(s,:),mu_ML);

%                 % assume the variance is known
%                 variance = sigmas(s,k)^2;
%                 % update Gaussian mean hyperparams
%                 mu_N(s,k) = (variance/(N*var_0(s,k)+variance))*mu_0(s,k) + ...
%                     (N*var_0(s,k)/(N*var_0(s,k)+variance))*mu_ML;
% 
%                 prec_N = 1/var_0(s,k) + N/variance;
%                 sigma_N(s,k) = 1/sqrt(prec_N);

                              
                % assume the variance is known
                v = sigmas(s,k)^2; % previous estimate
                variance = v;
                v_0 = var_0(s,k);  % variance of prior mean
                
                p = 1/(N*v_0 + v);
                
                % update Gaussian mean hyperparams
                mu_N(s,k) = p*(v*mu_0(s,k) + N*v_0*mu_ML);
                
               %
               %mu_N(s,k) = (variance/(N*var_0(s,k)+variance))*mu_0(s,k) + ...
               %    (N*var_0(s,k)/(N*var_0(s,k)+variance))*mu_ML;
                
                prec_N = 1/v_0 + N/v;               
                sigma_N(s,k) = 1/sqrt(prec_N);
                    

                % update Gaussian variance hyperparams
                a_N(s,k) = a_0(s,k) + N/2;
                b_N(s,k) = b_0(s,k)+(N*var_ML)/2;

            end
        end
    end
