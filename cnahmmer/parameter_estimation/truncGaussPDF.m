function [P] = truncGaussPDF(x, mu, sigma2, a, b)
% truncGaussPDF Density function of a Gaussian truncated between a and b
% Based on TruncatedGaussPDF from http://mahieddine.ichir.free.fr/Matlab/matlab.html
x0 = mu;
lambda = 1/(2*sigma2);
% P = K * exp(-lambda * (x - x0)�2) I_[a,b[
P = zeros(size(x));
P((x>=a) & (x<=b)) = exp(-lambda*((x((x>=a) & (x<=b))-x0).^2));
K = TruncatedGaussCst(lambda,x0,a,b);
P = P/K;