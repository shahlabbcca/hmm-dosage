function rho = distancePrior(d,L)
    ex = exp(-d/(2*L))
    rho = 0.5*(1-ex);
    