function x = samplingFromOneSideTruncatedInverseGamma(a, b, right)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input:
%   a is the shape parameter of the inverse gamma distribution.
%   b is the scale parameter of the inverse gamma distribution.
%   right is the up bound of x.
%
% Output:
%   x is the random number following truncated inverse gamma, 
%   and x < right.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This method is based on book "Non-Uniform Random Variate Generation"
% by Devroye (1986) 

left = 1/right;

y = samplingFromOneSideTruncatedGamma(a, 1/b, left);

x = 1/y;