function w = dirichletMixtureLocalEv(observedCounts,priors)
    [P,K,N] = size(priors);
    w = zeros(P,N);
  
    for p = 1:P
        pr = reshape(priors(p,:,:),[K,N]);
        w(p,:) = dirichletpdfV(observedCounts,pr);
%        w(p,:) = dirichletpdfV(theta,pr);
    end
    
    w = normalise(w,1);
