function x = sampleV(p, n)
%SAMPLE    Sample from categorical distribution.
% SAMPLE(P,N) returns a row vector of N integers, sampled according to the 
% probability distribution P (an array of numbers >= 0, whose sum is > 0).
% sum(P) does not have to be 1, but it must be > 0.

% Written by Tom Minka 
% modified by Sohrab SHah
% (c) Microsoft Corporation. All rights reserved.
[K,N] = size(p);
if nargin < 2
  n = 1;
end

cdf = cumsum(p,1);
if any(cdf(end,:) <= 0)
    error('distribution is all zeros');
end
x = zeros(n,N);
for i = 1:n
  x(i,:) = sum(cdf < repmat(rand(1,N).*cdf(end,:),[K,1])) + 1;
end
