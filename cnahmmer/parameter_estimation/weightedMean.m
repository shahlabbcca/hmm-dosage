function [mu,N] = weightedMean(pZ,Y)
    N =  sum(pZ);
    mu = nansum(pZ.*Y)/N;
    