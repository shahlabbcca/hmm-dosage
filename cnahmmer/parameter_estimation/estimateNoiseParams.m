function [mus_hat, sigmas_hat] = estimateGaussianNoiseParams(mu_N,sigma_N,a_N,b_N)
    % function estimateNoiseParams
    % ---------------------------------------------------------------------
    % this function uses the updating equations for a Normal-Gamma
    % distribution as described by Bishop, Pattern Recognition and 
    % Machine Learning, pgs 97-102
    % Parameterization is mu       ~ Gauss(mu_N,sigma_N)
    %                    1/sigma^2 ~ Gamma(a_N,b_N)
    % return the MAP estimate given the updated hyperparameters
    mus_hat = mu_N;
    lambda  = (a_N-1)./b_N;
    sigmas_hat = sqrt(ones(size(mu_N))./lambda);
    