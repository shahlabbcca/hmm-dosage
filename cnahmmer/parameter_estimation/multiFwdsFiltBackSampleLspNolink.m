function [samples,gamma,loglik] = multiFwdsFiltBackSampleLspNolink(init_state_distrib, transmat, transmatLsp, obslik, defaultState, index)

% forwards propagation, backwards sampling using a non-stationary 
% transition matrix
%
% input
% init_state_distrib(q) 1-by-S (S is number of states)
% transmat(q, q')       1-by-N cell of S-by-S trans matrix
% obslik(q, t)          numS-by-S-by-N (numS is number of samples)
%
% output
% samples(s, t)  = discrete state sequence for sample s at time  t
[numS Q T] = size(obslik);
scale = ones(numS,T);

alpha = zeros(numS,Q,T,'single');
beta  = zeros(numS,Q,T,'single');
gamma = zeros(numS,Q,T,'single');
%trans = transmat;
samples = zeros(numS,T);

% do the forwards filtering pass
t = 1;
rinit = repmat(init_state_distrib,numS,1);
alpha(:,:,1) = rinit .* obslik(:,:,t);
[alpha(:,:,t), scale(:,t)] = normalize(alpha(:,:,t),2);


for t=2:T    
  trans = transmat;
  if index(t)~=defaultState
      trans = transmatLsp{index(t)};
  end
  m = trans' * alpha(:,:,t-1)';
  alpha(:,:,t) = m' .* obslik(:,:,t);
  [alpha(:,:,t), scale(:,t)] = normalize(alpha(:,:,t),2);
  %assert(approxeq(sum(alpha(:,t)),1))
end
loglik = sum(sum(log(scale)));

% do the backwards sampling pass
beta(:,:,T) = ones(numS,Q);
gamma(:,:,T) = alpha(:,:,T);
for s = 1:numS
    samples(s,t) = sample(gamma(s,:,T),1);
end
for t=T-1:-1:1
 trans = transmat;
 if index(t)~=defaultState
     trans = transmatLsp{index(t)};
 end
%  b = beta(:,:,t+1) .* obslik(:,:,t+1);
%  beta(:,:,t) = normalize((trans * b')',2);
%  gamma(:,:,t) = normalize(alpha(:,:,t) .* beta(:,:,t),2);

 % can this loop be avoided?
 for s = 1:numS
    xi_filtered = (alpha(s,:,t)' *  obslik(s,:,t+1)) .* trans;
    dist = xi_filtered(:,samples(s,t+1))+1e-8;
    samples(s,t)=sample(dist);
 end
end
