function x = samplingFromOneSideTruncatedGamma(a, b, left)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input:
%   a is the shape parameter of the gamma distribution.
%   b is the scale parameter of the gamma distribution.
%   left is the low bound of x.
%
% Output:
%   x is the random number following truncated gamma with mean ab, 
%   and x > left.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This method is based on book "Non-Uniform Random Variate Generation"
% by Devroye (1986) 
low = left/b;

if a < 1
    e = exprnd(1);
    x = low + e;
    u = rand;
    while (log(x)+log(u)/(1-a)) > log(a)
        e = exprnd(1);
        x = low + e;
        u = rand;
    end
    
elseif a == 1
    e = exprnd(1);
    x = low + e;
else
    if low > (a-1)
        e1 = exprnd(1);
        e2 = exprnd(1);
        x = low + e1/(1-(a-1)/low);
        while (x/low-1+log(low/x)) > (e2/(a-1))
            e1 = exprnd(1);
            e2 = exprnd(1);
            x = low + e1/(1-(a-1)/low);
        end
    else
        x = gamrnd(a,1);
        while x <= low
            x = gamrnd(a,1);
        end
    end    
end

x = x*b;
