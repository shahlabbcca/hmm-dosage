function p = dirichletMixturePosterior(observedCounts, priors, weights)
    % function p = dirichletMixturePosterior(observedCounts, priors, weights)
    % observedCounts  - K-by-N matrix of counts
    % priors          - P-by-K-by-N matrix of priors - can be
    %                   non-stationary
    % weights         - P-by-N mixture component weights (sum to one across
    %                   rows)
    
    
    [P,K,N] = size(priors);
    p = zeros(K,N);
    weights = reshape(repmat(weights,[K,1]),[P,K,N]);
    
    p = normalise(observedCounts + reshape(sum(priors.*weights),K,N),1);
 