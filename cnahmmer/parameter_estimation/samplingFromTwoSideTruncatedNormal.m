function x = samplingFromTwoSideTruncatedNormal(m, sd, left, right)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input:
%   m is the mean of the normal distribution.
%   sd is the standard deviation of the normal distribution.
%   left is the lower bound of x.
%   right is the upper bound of x.
%
% Output:
%   x is the random number following truncated normal with mean m, 
%   standard deviation sd and left < x < right.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This method is based on paper "Simulation of truncated normal variables"
% by Robert (1995) 

% Standardize 
low = (left - m)/sd;
up = (right - m)/sd;

u =1; prob = 0;
while u > prob
    % z follows uniform distribution on [low, up]
    z = rand*(up-low)+low;
    % compute the acceptance probability
    if low > 0
        prob = exp((low*low-z*z)/2);    
    elseif up < 0
        prob = exp((up*up-z*z)/2);
    else
        prob = exp(-(z*z/2));
    end    
    u = rand;
end

x = m + z*sd;