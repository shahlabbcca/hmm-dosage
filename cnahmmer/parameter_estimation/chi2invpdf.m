function y = chi2invpdf(x,nu,sigma)
    % FUNCTION: chi2invpdf
    %           implements the Inverse Chi Squared probability density
    %           function with DOF parameter nu and Shape parameter sigma
    %           NOTE: x must be strictly positive
    y=x.^(-nu/2-1).*exp(-0.5*(nu*sigma)*x.^(-1));
        