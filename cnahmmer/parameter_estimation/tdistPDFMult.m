function localEvidence = tdistPDFMult(Y,mus,lambdas,nu)
    [P,N] = size(Y);
    [P,K] = size(mus);
    
    localEvidence = zeros(P,K,N);
    % calculate Students-t PDF for each row of Y
    for p = 1:P
        %disp(['Calculating local evidence for patient ',  int2str(p)]);
        for k = 1:K
            localEvidence(p,k,:) = tdistPDF(Y(p,:),mus(p,k),lambdas(p,k),nu(k));
        end
    end
    localEvidence = normalize(localEvidence,2);