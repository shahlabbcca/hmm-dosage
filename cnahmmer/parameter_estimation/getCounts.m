function Mcounts = getCounts(states,Nstate)

Mcounts = zeros(Nstate,Nstate);
if numel(states)==0
    return;
end

Ny = length(states);

if Ny < Nstate*Nstate
    before = states(1);
    for i = 2:Ny
        after = states(i);
        Mcounts(before,after) = Mcounts(before,after)+1;
        before = after;
    end
else
    twoStates = [states(1:(Ny-1))';states(2:Ny)'];
    for i = 1:Nstate
        for j = 1:Nstate
            Mcounts(i,j) = sum((twoStates(1,:)==i).*(twoStates(2,:)==j));
        end
    end
end
