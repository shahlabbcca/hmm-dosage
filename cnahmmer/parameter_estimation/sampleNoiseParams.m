function [mus_hat, sigmas_hat] = sampleNoiseParams(Y,Z,mu_0,var_0,a_0,b_0,sigmas)
    % function sampleNoiseParams
    % ---------------------------------------------------------------------
    % this function uses the updating equations for a Normal-Gamma
    % distribution as described by Bishop, Pattern Recognition and 
    % Machine Learning, pgs 97-102
    % Parameterization is 
    [numS,n] = size(Z);
    [numS,S] = size(mu_0);
    mus_hat = zeros(numS,S);
    sigmas_hat = zeros(numS,S);
    for s=1:numS
        for k=1:S
            indexState = find(Z(s,:)==k);
            N = length(indexState);
            if N == 0                
                mu_N = mu_0(s,k);
                sigma_N = sqrt(var_0(s,k));
                a_N = a_0(s,k);
                b_N = b_0(s,k);
            else
                % use conjugate Gaussian prior - assume variance is known
                % (from last iteration (given as sigmas)
                mu_ML = mean(Y(s,indexState));
                var_ML = var(Y(s,indexState));
                
                % assume the variance is known
                variance = sigmas(s,k)^2;
                % update Gaussian mean hyperparams
                mu_N = (variance/(N*var_0(s,k)+variance))*mu_0(s,k) + ...
                    (N*var_0(s,k)/(N*var_0(s,k)+variance))*mu_ML;
                
                prec_N = 1/var_0(s,k) + N/variance;               
                sigma_N = 1/sqrt(prec_N);
                                
                % update Gaussian variance hyperparams
                a_N = a_0(s,k) + N/2;   
                b_N = b_0(s,k)+(N*var_ML)/2;            
            end
            
            % now sample from the gaussian to get the mean
            mus_hat(s,k) = normrnd(mu_N,sigma_N);

            % sample from the gamma and calculate the std dev
            lambda = gamrnd(a_N,1/b_N);
            sigmas_hat(s,k) = sqrt(1/lambda);
        end
    end
    
    mus_hat
    sigmas_hat