function Mcounts = getColumnCounts(states,Nstate)

Mcounts = zeros(Nstate,1);
for i=1:Nstate
    Mcounts(i) = sum(states==i);
end

