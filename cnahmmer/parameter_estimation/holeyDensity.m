function py = holeyDensity(y,mu,sigma)
    % FUNCTION: holeyDensity(y,mu,sigma)
    %           implements a distribution that is meant to be a symmetrical
    %           Inverse Chi Squared distribution centered at mu and peaked
    %           at mu+2sigma.  This models the PDF of the outlier of a
    %           state with mean mu and variance sigma
    py = invChiSquaredPdf(y,sigma,4.01,mu);
    % The 4.01 is currently hard-coded - this parameter should be learned 
    py(find(isnan(py))) = 0;