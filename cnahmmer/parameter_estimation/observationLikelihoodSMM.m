function obslik = observationLikelihoodSMM(Y,mus,sigmas,epsilon)
    [numS, N] = size(Y);
    [M,K] = size(mus);

    obslikZ = zeros(numS,K,N);
    obslik = zeros(numS,2*K,N);
    for i=1:numS
        obslikZ(i,:,:) = GaussObs(Y(i,:),mus(i,:),sigmas(i,:));
        for k = 1:K
            obslik(i,k*2-1,:) = obslikZ(i,k,:);%.*reshape(epsilon(1,:),[1,1,N]);
            obslik(i,k*2,:) = obslikZ(i,k,:);%.*reshape(epsilon(2,:),[1,1,N]);
        end
    end
%     obslikZ = reshape(repmat(obslikZ,[2,1,1]),[numS,2*K,N]);
%     epsilont = repmat(epsilon([numS,1]))
%     epsilont = reshape(repmat(reshape(epsilon,[numS,1]),[,[numS,2*K,N]);
%     %epsilon = reshape(repmat(epsilon,[numS,numS,1]),[numS,2*K,N]);
%     obslik = obslikZ.*epsilont;
%     
%         for k = 1:K
%             obslik(i,k*2-1,:) = obslikZ(i,k,:);
%             obslik(i,k*2,:) = obslikZ(i,k,:);
%         end     
%     end 
    %obslik = reshape(repmat(obslikZ,[1,2,1]),[numS,K*2,N]);
