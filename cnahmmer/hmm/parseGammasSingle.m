function G = parseGammasSingle(ZL,sizes)
    
    [S,N] = size(ZL);
    G = cell(1,length(sizes));
    I = ind2subv(sizes,1:prod(sizes));
    for i=1:length(sizes)
        G{i}=zeros(sizes(i),N);
        for j=1:sizes(i)
            state = find(I(:,i)==j);           
            G{i}(j,:)=sum(ZL(state,:),1);
        end
    end
            
    