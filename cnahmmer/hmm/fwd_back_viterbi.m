function [path,gamma,eta,loglik] = fwd_back_viterbi(init_state_distrib,transmat,obslik)

% forwards backwards and Viterbi for M->Z->Y HMM where Z is integrated out.
% gammaZ contains the marginals on Z for further parameter estimation 
% of Y
%
% input
% init_state_distrib(q)
% transmat(q, q')
% obslik(q, t)
% nsamples
%
% output
% samples(t, s)  = sample s of discrete state at time  t


[Q T] = size(obslik);
scale = ones(1,T);
loglik = 0;
alpha = zeros(Q,T, 'single');
delta = zeros(Q,T);
psi = zeros(Q,T);
path = zeros(1,T);
gamma = zeros(Q,T,'single');
eta   = zeros(Q,Q,T,'single');
trans = transmat;

t = 1;
delta(:,t) = log(init_state_distrib) + log(obslik(:,t));
alpha(:,1) = init_state_distrib(:) .* obslik(:,t);
[alpha(:,t), scale(t)] = normalize(alpha(:,t));
psi(:,t) = 0;
for t=2:T
    for j=1:Q
        [delta(j,t), psi(j,t)] = max(delta(:,t-1) + log(transmat(:,j)));
        delta(j,t) = delta(j,t) + log(obslik(j,t));
    end

    m = trans' * alpha(:,t-1);
    alpha(:,t) = m(:) .* obslik(:,t);
    [alpha(:,t), scale(t)] = normalize(alpha(:,t));
end


beta = zeros(Q,T);
t=T;
beta(:,T) = ones(Q,1);
gamma(:,T) = alpha(:,T);

[loglik, path(T)] = max(delta(:,T));

for t=T-1:-1:1
    path(t) = psi(path(t+1),t+1);
    b = beta(:,t+1) .* obslik(:,t+1);
    beta(:,t) = normalize(transmat * b);
    gamma(:,t) = normalize(alpha(:,t) .* beta(:,t));
    eta(:,:,t) = normalise((transmat .* (alpha(:,t) * b')));
end

