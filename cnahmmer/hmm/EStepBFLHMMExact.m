function [pM,pZ,mCounts] = EStepBFLHMMExact(Y,Ae,Am,piM,mus,sigmas,chrIndex)
    % function to do the E-step
    % Y      - numS-by-N data
    % Ae     - diagonal matrix with epsilon on the diag
    % Am     - transition matrix for master (S+1-by-S+1)
    % mus    - current estimate of mu^s (numS-by-3)
    % sigmas - current estimate of sigma^s
    chrs = unique(chrIndex);
    S = length(Am);
    [numS,N] = size(Y);
    M = zeros(1,N);
    mCounts = zeros(S);
    pM = zeros(S,N);
    pZ = zeros(numS,S,N);
    % compute the local evidence for the slaves
    obslikZ = zeros(numS,S,N);
        
    for i=1:numS
        obslikZ(i,:,:) = GaussObs(Y(i,:),mus(i,:),sigmas(i,:));
    end
       
    % compute the local evidence for the master
    obsMaster = masterLocalEvidenceBFL(Ae,obslikZ);
    for c=1:length(chrs)
        disp(['chromosome ', int2str(c)]);
        I = find(chrIndex == chrs(c));
        % sample the master given the slave
        [M(I), pM(:,I), pZ(:,:,I)] = fwdsFiltBackSampleBFL(piM,Am,Ae,obsMaster(:,I),obslikZ(:,:,I),1);
        % update the counts
        [junk, M(I)] = max(pM(:,I));
        mCounts = mCounts + getCounts(M(I)',S);
    end

    
            
            
            