function bf = computeSegmentBayesFactor(k,A,p)
% function bf = computeSegmentBayesFactor(k,A,p)
% compute a Bayes Factor (likelihood ratio?) of the
% predicted segment by Viterbi over all possible segments
% k  - state of segment
% A  - converged K-by-K transition matrix
% p  - observation likelihood (local evidence)
    pi = stationaryDistribution(A)';
    piNum = zeros(size(pi));
    piNum(k) = 1;
    ANum = eye(length(pi));
    [anum num] = fwds(piNum,ANum,p);
    [aden den] = fwds(pi,A,p);
    bf = exp(num)/exp(den);
    
    
    