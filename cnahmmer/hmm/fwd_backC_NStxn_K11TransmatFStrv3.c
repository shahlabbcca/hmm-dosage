#include <math.h>
#include "mex.h"
#include <string.h>

double normalizeInPlace(double *, unsigned int);
void multiplyInPlace(double *, double *, double *, unsigned int);
void multiplyMatrixInPlace(double *, double *, double *, unsigned int);
void transposeSquareInPlace(double *, double *, unsigned int);
void outerProductUVInPlace(double *, double *, double *, unsigned int);
void componentVectorMultiplyInPlace(double *, double *, double *, unsigned int);
void preparePositionSpecificMatrix(double * transSlice, double * frequencies, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, double, double, unsigned int);
void deepCopy(double * transSlice, double* transmat, unsigned int K);
void outputMatrix(double * A, unsigned int K);
/*
 * To test in Matlab with the usual fwd_back that has 4 outputs :
 * init_states = rand(4,1);
 * trans = rand(4,4);
 * obslik = rand(4,10);
 * [E1,E2,E3,E4] = fwd_backC(init_states, trans, obslik);
 * [F1,F2,F3,F4] = fwd_back(init_states, trans, obslik);
 * max(max(abs(E1 - F1)))
 * max(max(abs(E2 - F2)))
 * max(max(abs(E3 - F3)))
 * max(max(abs(E4 - F4)))
 *
 * or this with the fwd_back version that returns eta also :
 * init_states = rand(4,1);
 * trans = rand(4,4);
 * obslik = rand(4,10);
 * [F1,F2,F3,F4,F5] = fwd_backC(init_states, trans, obslik);
 * [E1,E2,E3,E4,E5] = fwd_back(init_states, trans, obslik);
 * max(max(abs(E1 - F1)))
 * max(max(abs(E2 - F2)))
 * max(max(abs(E3 - F3)))
 * max(abs(E4(:) - F4(:)))
 * max(max(abs(E5 - F5)))
 *
 */

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    double * init_state_distrib, * transmat, * obslik;
    int K, T, tmp, numFreqState, tmp1;
    double * KcnvL, * KcnvG, * zeroFreqBool, * cnvStrength, * nonCNVStrength;
    
    /* the tranposed version of transmat*/
    double * transmatT, * transSlice, * frequencies;
    
    double * scale, * alpha, * beta, * gamma;
    int t, d, i, j;
    double loglik = 0;
    
    /* special care for eta since it has 3 dimensions and needs to be written
     * with mxCreateNumericArray instead of mxCreateDoubleMatrix */
    /*double * eta;
     * int eta_ndim = 3;
     * int * eta_dims = mxMalloc(eta_ndim*sizeof(int));
     */
    double *m, *b; /*, *squareSpace; /* temporary quantities in the algorithm */
    double *outputToolPtr;
    
    if (nrhs!=9 || !(nlhs==4 || nlhs==5))
        mexErrMsgTxt("fwd_backC: requires 9 inputs and 4-5 outputs");
    
    init_state_distrib=mxGetPr(prhs[0]);
    transmat=mxGetPr(prhs[1]); /* base matrix - stationary values, KxK */
    obslik=mxGetPr(prhs[2]);
    frequencies=mxGetPr(prhs[3]);/* 2 rows, length (T-1) - del and amp */
    cnvStrength=mxGetPr(prhs[4]);/* integer */
    nonCNVStrength=mxGetPr(prhs[5]);/* integer */
    KcnvL=mxGetPr(prhs[6]);/* integer */
    KcnvG=mxGetPr(prhs[7]);/* integer */
    zeroFreqBool=mxGetPr(prhs[8]);/* integer */
    
    /* mexPrintf("fwd_backC: Number of CNV states: loss-%f \t gain-%f\n",KcnvL[0],KcnvG[0]); */
    /* mexPrintf("fwd_backC: Strengths: CNV-%f \t NonCNV-%f\n",cnvStrength[0],nonCNVStrength[0]); */
    
    /* Assuming this will allow me to take row or colums as arguments. */
    K = mxGetN(prhs[0]) * mxGetM(prhs[0]); /* Total number of states */
    
    obslik = mxGetPr(prhs[2]);
    /* I might have switched M and N.*/
    T = mxGetN(prhs[2]);
    tmp = mxGetM(prhs[1]);
    if (tmp != K)
        mexErrMsgTxt("fwd_backC: The obslik must have K rows.");
    
    tmp1 = mxGetN(prhs[3]);
    numFreqState = mxGetM(prhs[3]);
    if (tmp1 != T-1)
        mexErrMsgTxt("fwd_backC: The frequencies need to be 2x(T-1).");
    /* mexPrintf("fwd_backC: Frequencies is %d-by-%d\n",numFreqState,tmp1); */
    
    tmp = mxGetN(prhs[1]);
    if (tmp != K)
        mexErrMsgTxt("fwd_backC: The transition matrix must be of size KxK.");
    /*mexPrintf("Txn matrix is %d-by-%d\n",mxGetM(prhs[1]),tmp);*/
    
    scale = mxMalloc(T*sizeof(double));
    
    alpha = mxMalloc(K*T*sizeof(double));
    beta = mxMalloc(K*T*sizeof(double));
    gamma = mxMalloc(K*T*sizeof(double));
    
    /* Use transSlice as the temporary txn matrix to modify
     * with cnv frequencies; each iteration, we overwrite it with transmat
     * to start over at a new probe */
    transSlice = mxMalloc(K*K*sizeof(double));
    
    transmatT = mxMalloc(K*K*sizeof(double));
    
    
    /********* Forward. ********/
    
    t = 0;
    multiplyInPlace(alpha + t*K, init_state_distrib, obslik + t*K, K);
    /*mexPrintf("Normalize alpha at t=%d\n",t);*/
    scale[t] = normalizeInPlace(alpha + t*K, K);
    
    m = mxMalloc(K*sizeof(double));
    
    for(t=1;t<T;++t){
        /* Each iteration, we overwrite transSlice with transmat
         * to start over when at a new probe */
        deepCopy(transSlice, transmat, K);
        /* modify transSlice inplace by adding cnv frequencies*/
        preparePositionSpecificMatrix(transSlice, frequencies + (t-1)*numFreqState, numFreqState, K, KcnvL[0], KcnvG[0], zeroFreqBool[0], cnvStrength[0], nonCNVStrength[0], 0);
        /* transposeSquareInPlace(transmatT, transmat + K*K*(t-1), K); */
        transposeSquareInPlace(transmatT, transSlice, K);
        multiplyMatrixInPlace(m, transmatT, alpha + (t-1)*K, K);
        multiplyInPlace(alpha + t*K, m, obslik + t*K, K);
        /*mexPrintf("Normalize alpha at t=%d\tSum=%f\n",t,sum);*/
        scale[t] = normalizeInPlace(alpha + t*K, K);
    }
    
    loglik = 0;
    for(t=0;t<T;++t)
        loglik += log(scale[t]);
    
    /********* Backward. ********/
    
    
    t = T-1;
    /* I don't think we need to initialize beta to all zeros. */
    for(d=0;d<K;++d) {
        beta[d + t*K] = 1;
        gamma[d + t*K] = alpha[d + t*K];
    }
    
    b = mxMalloc(K*sizeof(double));/*mxCreateDoubleMatrix(K,1,mxREAL);*/
    /*    eta = mxMalloc(K*K*T*sizeof(double));
     * squareSpace = mxMalloc(K*K*sizeof(double));
     */
    /* Put the last slice of eta as zeros, to be compatible with Sohrab and Gavin's code.
     * There are no values to put there anyways. This means that you can't normalise the
     * last matrix in eta, but it shouldn't be used. Note the d<K*K range.
     */
    /*
     * for(d=0;d<(K*K);++d) {
     * mexPrintf("setting *(eta + %d) = 0 \n", d+t*K*K);
     *(eta + d + t*K*K) = 0;(double)7.0f;
     * }
     */
    /* We have to remember that the 1:T range in Matlab is 0:(T-1) in C. */
    for(t=(T-2);t>=0;--t) {
        
        /* setting beta */
        multiplyInPlace(b, beta + (t+1)*K, obslik + (t+1)*K, K);
        /* Using "m" again instead of defining a new temporary variable.
         * We using a lot of lines to say
         * beta(:,t) = normalize(transmat * b);
         */
        deepCopy(transSlice, transmat, K);
        preparePositionSpecificMatrix(transSlice, frequencies + t*numFreqState, numFreqState, K, KcnvL[0], KcnvG[0], zeroFreqBool[0], cnvStrength[0], nonCNVStrength[0], 0);
        multiplyMatrixInPlace(m, transSlice, b, K);
        normalizeInPlace(m, K);
        for(d=0;d<K;++d) { beta[d + t*K] = m[d]; }
        /* using "m" again as valueholder */
        
        /* setting eta, whether we want it or not in the output */
        /*	    outerProductUVInPlace(squareSpace, alpha + t*K, b, K);
         * componentVectorMultiplyInPlace(eta + t*K*K, transmat + K*K*t, squareSpace, K*K);
         * normalizeInPlace(eta + t*K*K, K*K);
         */
        /* setting gamma */
        
        multiplyInPlace(m, alpha + t*K, beta + t*K, K);
        normalizeInPlace(m, K);
        for(d=0;d<K;++d) { gamma[d + t*K] = m[d]; }
    }
    
    
    
    plhs[0] = mxCreateDoubleMatrix(K, T, mxREAL);
    outputToolPtr = mxGetPr(plhs[0]);
    memcpy(outputToolPtr, gamma, K*T*sizeof(double));
    
    plhs[1] = mxCreateDoubleMatrix(K, T, mxREAL);
    outputToolPtr = mxGetPr(plhs[1]);
    memcpy(outputToolPtr, alpha, K*T*sizeof(double));
    
    plhs[2] = mxCreateDoubleMatrix(K, T, mxREAL);
    outputToolPtr = mxGetPr(plhs[2]);
    memcpy(outputToolPtr, beta, K*T*sizeof(double));
    
    /* This handles the two possible cases for outputs, based on the number of outputs.
     * It's either
     * [gamma,alpha,beta,eta,loglik]
     * or
     * [gamma,alpha,beta,loglik].
     */
    if (nlhs == 4) {
        plhs[3] = mxCreateDoubleMatrix(1, 1, mxREAL);
        outputToolPtr = mxGetPr(plhs[3]);
        outputToolPtr[0] = loglik;
        /*  } else if (nlhs == 5) {
         * eta_dims[0] = K;
         * eta_dims[1] = K;
         * eta_dims[2] = T;
         * plhs[3] = mxCreateNumericArray(eta_ndim, eta_dims, mxDOUBLE_CLASS, mxREAL);
         * outputToolPtr = mxGetPr(plhs[3]);
         * memcpy(outputToolPtr, eta, K*K*T*sizeof(double));
         *
         *
         * plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);
         * outputToolPtr = mxGetPr(plhs[4]);
         * outputToolPtr[0] = loglik;
         */
    }
    
    
    mxFree(b); mxFree(m); /*mxFree(squareSpace);*/
    mxFree(scale); mxFree(transmatT);
    mxFree(alpha); mxFree(beta); mxFree(gamma); /*mxFree(eta); mxFree(eta_dims);*/
    
    return;
}

/* Method to assign transSlice as the position specific matrix that includes cnv freq prior
 * A(1:K-2,K-1:K,j) = A(1:K-2,K-1:K,j) + repmat(F(j,:),[K-2 1]); */
void preparePositionSpecificMatrix(double * transSlice, double * frequencies, unsigned int numFreqState, unsigned int K, unsigned int KcnvL, unsigned int KcnvG, unsigned int zeroFreqBool, double cnvStrength, double nonCNVStrength, unsigned int boolTest) {
    /* cnv(t) del =  frequencies[0] */
    /* cnv(t) amp =  frequencies[1] */
    unsigned int i, j, colCount;
    double totalFreqAtCurPosn, nonCNVFreq;
    totalFreqAtCurPosn = 0;
    /* Add the frequencies to our output matrix, in place */
    /* A(1:K,K-Kcnv:K,j) = A(1:K,K-Kcnv:K,j) + repmat(F(j,:),[K 1]); */
    colCount = 0;
    for (j=K-KcnvL-KcnvG;j<K-KcnvG;j++){ /* columns - only CNV loss columns */
        for (i=0;i<K;i++){  /* rows - all K rows*/
            /*mexPrintf("frqeuency: %f\tcolumn: %d\n",frequencies[colCount],colCount);*/
            if (numFreqState==2){
                if (frequencies[0]==0 && zeroFreqBool==1){
                    transSlice[i + j*K] = 0;
                }else{
                    transSlice[i + j*K] += (frequencies[0])*cnvStrength;
                }
            }else{
                if (frequencies[colCount]==0 && zeroFreqBool==1) {
                    transSlice[i + j*K] = 0;
                } else {
                    transSlice[i + j*K] += frequencies[colCount]*cnvStrength;
                }
            }
        }
        colCount += 1;
    }
    
    for (j=K-KcnvG;j<K;j++){ /* columns - only CNV gain columns */
        for (i=0;i<K;i++){  /* rows - all K rows*/
            /*mexPrintf("frqeuency: %f\tcolumn: %d\n",frequencies[colCount],colCount);*/
            if (numFreqState==2){
                if (frequencies[1]==0 && zeroFreqBool==1){
                    transSlice[i + j*K] = 0;
                }else{
                    transSlice[i + j*K] += (frequencies[1])*cnvStrength;
                }
            }else{
                if (frequencies[colCount]==0 && zeroFreqBool==1){
                    transSlice[i + j*K] = 0;
                } else {
                    transSlice[i + j*K] += frequencies[colCount]*cnvStrength;
                }
            }
        }
        colCount += 1;
    }
    
    /* Add the non-CNV frequencies to our output matrix, in place */
    
    for (i=0; i<numFreqState;i++){
        totalFreqAtCurPosn += frequencies[i];
    }
    
    /* A(1:K,1:K-Kcnv,j) = A(1:K,1:K-Kcnv,j) + repmat((1-(sum(F(j,:))))/K-Kcnv,[K 1]); */
    nonCNVFreq = (1-(totalFreqAtCurPosn)) / ((K-KcnvL-KcnvG));
    if (nonCNVFreq < 0) { nonCNVFreq = 0;}
    for (j=0;j<K-KcnvL-KcnvG;j++){ /* columns - non-CNV columns */
        for (i=0;i<K;i++){  /* rows - all K rows */
            /*mexPrintf("frqeuency: %f\tcolumn: %d\n",frequencies[colCount],colCount);*/
            transSlice[i + j*K] += (nonCNVFreq/(K-KcnvL-KcnvG))*nonCNVStrength;
        }
    }
    
    if (boolTest) {
        mexPrintf("fwd_backC: \n");
        for (i=0;i<numFreqState;i++){
            mexPrintf("freq%i=%f\t", i, frequencies[i]);
        }
        mexPrintf("TotalFreq:%f\tnonCNV:%f\n", totalFreqAtCurPosn, nonCNVFreq);
        mexPrintf("fwd_backC: Raw Matrix: \n");
        outputMatrix(transSlice, K);
    }
    
    /* Normalize matrix by rows */
    double sum;
    
    for (i=0;i<K;i++) { /* rows */
        sum = 0;
        for (j=0;j<K;j++) { /* columns */
            sum += transSlice[i+j*K];
        }
        for (j=0;j<K;j++) {
            
            transSlice[i+j*K] /= sum;
        }
        /*mexPrintf("Sum: %f\n",sum);*/
        
    }
    
    if (boolTest) {
        mexPrintf("fwd_backC: Normalized Matrix: \n");
        outputMatrix(transSlice, K);
    }
}

void deepCopy(double * transSlice, double* transmat, unsigned int K) {
    unsigned int i, j;
    
    /* deep copy transmat to transSlice */
    for (i=0;i<K;i++) /* rows */
        for (j=0;j<K;j++) /* columns */
            transSlice[i + j*K] = transmat[i + j*K];
}

/* Output matrix for debugging purposes */
void outputMatrix(double * A, unsigned int K) {
    unsigned int i, j;
    
    for (i=0;i<K;i++) { /*rows*/
        for (j=0;j<K;j++) { /*cols*/
            mexPrintf("%f\t", A[K*j+i]);
        }
        mexPrintf("\n");
    }
    
}

/* And returns the normalization constant used.
 * I'm assuming that all I'll want to do is to normalize columns
 * so I don't need to include a stride variable.
 */
double normalizeInPlace(double * A, unsigned int N) {
    unsigned int n;
    double sum = 0;
    
    for(n=0;n<N;++n) {
        sum += A[n];
        if (A[n] < 0) {
            mexErrMsgTxt("We don't want to normalize if A contains a negative value. This is a logical error.");
        }
    }
    
    if (sum == 0)
        mexErrMsgTxt("We are asked to normalize a section of a vector containing only zeros.");
    else {
        for(n=0;n<N;++n)
            A[n] /= sum;
    }
    return sum;
}

void multiplyInPlace(double * result, double * u, double * v, unsigned int K) {
    unsigned int n;
    
    for(n=0;n<K;++n)
        result[n] = u[n] * v[n];
    
    return;
}

void multiplyMatrixInPlace(double * result, double * trans, double * v, unsigned int K) {
    
    unsigned int i, d;
    
    for(d=0;d<K;++d) {
        result[d] = 0;
        for (i=0;i<K;++i){
            result[d] += trans[d + i*K] * v[i];
        }
    }
    return;
}

void transposeSquareInPlace(double * out, double * in, unsigned int K) {
    
    unsigned int i, j;
    
    for(i=0;i<K;++i){
        for(j=0;j<K;++j){
            out[j+i*K] = in[i+j*K];
        }
    }
    return;
}

void outerProductUVInPlace(double * Out, double * u, double * v, unsigned int K) {
    unsigned int i, j;
    
    for(i=0;i<K;++i){
        for(j=0;j<K;++j){
            Out[i + j*K] = u[i] * v[j];
        }
    }
    return;
}

/* this works for matrices also if you just set the length "L" to be the right value,
 * often K*K, instead of just K in the case of vectors
 */
void componentVectorMultiplyInPlace(double * Out, double * u, double * v, unsigned int L) {
    unsigned int i;
    
    for(i=0;i<L;++i)
        Out[i] = u[i] * v[i];
    
    return;
}
