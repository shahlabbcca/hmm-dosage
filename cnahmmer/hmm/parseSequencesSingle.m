function S = parseSequencesSingle(Z,sizes)
    
    [N] = length(Z);
    S = cell(1,length(sizes));
    I = ind2subv(sizes,1:prod(sizes));
    for i=1:length(sizes)
        S{i}=zeros(1,N);
        for j=1:sizes(i)
            state = find(I(:,i)==j);
            ind=find(ismember(Z,state));
            S{i}(ind)=j;
        end
    end
            
    