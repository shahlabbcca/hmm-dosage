function [mus,sigmas,pZ,loglik] = ...
    hmmr(Y,chrIndex,mu_0,var_0,a_0,b_0,a_prior,b_prior,out_eps,maxiter)
% FUNCTION:
% =========
% hmmr(Y,chrIndex,mu_0,var_0,a_0,b_0,a_prior,b_prior,out_eps,maxiter)
% 
% constants:
%------------
% N - length of the data
% K - number of states
%
% parameters:
%-------------
% Y        : data (1-by-N) (sorted by chromosome, then by position)
% chrIndex : 1-by-N vector to denote which chromosome the data belongs to
% mu_0     : mean hyperparameter for Gaussian mean (1-by-K)
% var_0    : variance hyperparameter for Gaussian mean (1-by-K) 
% a_0      : scale hyperparameter for Gamma on precision (1-by-K)
% b_0      : shape hyperparameter for Gamma on precision (1-by-K)
% a_prior  : hyperparam on Beta distrib on outliers (scalar)
% b_prior  : hyperparam on Beta distrib on outliers (scalar)
% out_eps  : prior probability of outlier->outlier transition (should be
%            0.01 or less)
% maxiter  : maximum number of iterations for EM algorithm
%
% NOTE: to update the noise parameters, this function uses equations given 
%       in Bishop p79-93
%
% RETURN:
% =======
% mus             - estimate of the state means for each iteration
% sigmas          - estimate of the state variance for each iteration
% states          - state sequences for each iteration 
% loglik          - p(y|theta) for each iteration
%
K = length(mu_0);
N = length(Y);
mus = zeros(K,maxiter);          % state means
sigmas = zeros(K,maxiter);       % state variances
converged = 0;                   % flag for convergence
loglik = zeros(1,maxiter);

% SET UP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set up the chromosome indicies
% make cell array of chromosome indicies
chrs = unique(chrIndex);
chrsI = cell(1,length(chrs));

% set up DBN to HMM transition matrix 
% this requires the pomdp code from Kevin Murphy
% see flattenTransModelDemo.m for usage examples 
sizes=[2 K]; % need to change this when considering CNPs
o1=1; z1=2; o2=3; z2=4;
o=1; z=2;
inlier=1;
outlier=2;
sizes2=[sizes sizes];
Ns = length(sizes);

piZ = cell(1,length(chrs)); % need 1 initial dist per chromosome
% initialise the chromosome index and the init state distributions 
for c = 1:length(chrs)
    chrsI{c} = find(chrIndex == chrs(c));
    piZ{c} = ones(1,K)/K;
end

% set up the transition table
transTable = cell(1,Ns); parents = cell(1,Ns);

% randomly initialise the outlier transition table by sampling from the
% prior
lambda=a_prior/(a_prior+b_prior);
%lambda=betarnd(a_prior,b_prior);
transTable{o}=[1-lambda, lambda; 1-out_eps, out_eps];
parents{o}=o1;
parents{z} = [z1, o2];
transTable{z} = zeros(sizes2([z1,o2,z2]));

% initialise the inlier transitions to the prior:
% A
strength=10;
e = 0.999;
A = zeros(K);
for j = 1:K
    A(j,:) = (1-e)/(K-1);
    A(j,j) = e;
end
A_prior = A;
dirPrior = A*strength;

% force state latching when current location is an outlier
transTable{z}(:,inlier,:) = A; 
% force state latching when current location is an outlier
transTable{z}(:,outlier,:) = eye(K); 

% Parameters
% ========================================================================
lambdas = zeros(maxiter,1);

Z = zeros(N,1); % inlier state sequences
O = zeros(N,1); % outlier state sequences
pZ = zeros(K,N);

% probabilities to return
pO = zeros(2,N);     %   p(O_t=k|theta,data)
ZO = zeros(N,1);  %   p(Z_t=k|theta,data)

% Initialization
% ========================================================================
i = 1;

lambdas(i)=lambda;
transTable{o}=[1-lambdas(i), lambdas(i); 1-out_eps, out_eps];

% flatten the DBN to a HMM transition matrix
Tflat = flattenTransModel(transTable, parents);

% initialise mu and sigma
i = 1;
[mus(:,i), sigmas(:,i)] = estimateGaussianNoiseParams(mu_0,var_0,a_0,b_0);
pi_init = ones(prod(sizes),1);
piCounts = ones(prod(sizes),1)/prod(sizes);
piPrior = repmat([1/prod(sizes)],prod(sizes),1);
localEv = GaussRobustObs(Y,sizes,mus(:,i),sigmas(:,i))+1e-8;
loglik(i) = -Inf;

% Expectation Maximization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while (~converged && (i < maxiter))
    i=i+1;
    mess=sprintf('EM MAP iteration: %d  Loglik: %f',i,loglik(i-1));
    disp(mess);
    
    
    % for transition matrix updating
    Zcounts = zeros(K);
    Ocounts = zeros(2);
    % E-step
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % compute state sequences
    priorPi = 0;
    for c = 1:length(chrsI)
        
        piStat = normalise(piCounts);
        priorPi = priorPi+log(dirichletpdf(piPrior,piStat));
        piZ{c} = piStat;
        
        I = chrsI{c};
        % infer Z and O in a ZO mega variable
        [gamma,alpha,beta,eta,ll] = fwd_backC(piZ{c}, Tflat, localEv(:,I));
        
        [m ZO(I)] = max(gamma,[],1);
        % now decouple Z and O by marginalisation
        Q = parseSequencesSingle(ZO(I),sizes);        
        G = parseGammasSingle(gamma,sizes);
        
        Z(I) = Q{z};
        pZ(:,I) = G{z};
        
        O(I) = Q{o};
        pO(:,I) = G{o};
        
        Zin = Q{z}(find(Q{o}==inlier));  
        Zcounts = Zcounts+getCounts(Zin',K);   
        Ocounts = Ocounts+getCounts(O(I),2);
        loglik(i) = loglik(i)+ll;
    end
    
    %disp(Zcounts);
    % M-step
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update transition matrix A:
    priorA = 0;
    for j = 1:K
        A(j,:) = Zcounts(j,:)+dirPrior(j,:);
        A(j,:) = normalise(A(j,:));
        priorA = priorA + log(dirichletpdf(A_prior(j,:),A(j,:)));
    end
    A;
    % need to check this - how to evaluate the p(prior) for Dirichlet
    
    % set the non-outlier transitions to the standard transition matrix
    transTable{z}(:,inlier,:) = A;
    % update Bernoulli outlier parameter
    % inlier->outlier transition
    lambdas(i)=(a_prior+Ocounts(inlier,outlier))/(b_prior+a_prior+N-2);
    transTable{o}=[1-lambdas(i), lambdas(i); 1-out_eps, out_eps];
    
    % flatten the DBN to a HMM transition matrix
    Tflat = flattenTransModel(transTable, parents);
        
    % ------------------------------------------------------------------------
    % update emission density parameters
    priorMu = zeros(1,K);
    priorSigma = zeros(1,K);
    % only estimate noise params for inliers
    inI = find(O==inlier);
    %disp(size(inI));
    pZin = pZ(:,inI);
    Yin = Y(inI);
    Zin = Z(inI);
%    [mu_N, sigma_N, a_N, b_N] = ...
%       updateNoiseHyperparamsWeightedSS(Yin,pZin,mu_0,var_0,a_0,b_0,sigmas(:,i-1));
    [mu_N, sigma_N, a_N, b_N] = ...
       updateNoiseHyperparamsWeightedSS(Y,pZ,mu_0,var_0,a_0,b_0,sigmas(:,i-1));
%     [mu_N,sigma_N,a_N,b_N] = updateNoiseHyperparamsSS(Yin,Zin, ...
%             mu_0,var_0,a_0,b_0,sigmas(:,i-1));
%     
    [mus(:,i),sigmas(:,i)] = estimateGaussianNoiseParams(mu_N,sigma_N,a_N,b_N);
    %disp(mus(:,i));
    localEv = GaussRobustObs(Y,sizes,mus(:,i),sigmas(:,i))+1e-8;
    
    % compute log-likelihood and check convergence
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for k = 1:K
        % calculate the p(mu,sigma) = the prior of the normal-Gamma evaluated
        % at the estimated quantities.  See Bishop : pattern recognition and
        % machine learning, pg 101 for how to calculate this
        priorMu(k)      = log(normpdf(mus(k,i),mu_0(k),1));
        priorSigma(k)   = log(gampdf(1/sigmas(k,i)^2,a_0(k),b_0(k)));
    end
    %disp(sum(priorMu));
    %disp(sum(priorSigma));
    %disp(priorA);
    loglik(i) = loglik(i) + priorA + sum(priorMu) + sum(priorSigma);
    if (loglik(i) <= loglik(i-1))
        converged = 1;
    end
end

if converged
    i = i-1;
end
mus = mus(:,1:i);
sigmas = sigmas(:,1:i);
loglik = loglik(1:i);


