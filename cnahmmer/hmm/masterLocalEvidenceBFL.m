function py = masterLocalEvidenceBFL(Al,obslikZ)
    % masterLocalEvidenceHybrid(Al,Z)
    % 
    % FUNCTION
    % Calculate the local evidence to be used in fwds-bwds
    % to infer the master state sequence
    %
    % PARAMS
    % Al  : transition matrix for yoked slaves (S by S)
    % Z   : state sequence for slaves (numS by N \in {1:S})
    % where numS is the number of slaves
    %
    % RETURN
    % py : S by N matrix containing the local evidence
       
    [numS S N] = size(obslikZ); % number of slaves, length of the sequence
    S = length(Al);     % number of states the Sth state is don't care
     
    py = zeros(S,N);
    for t = 1:N
        for k=1:S
            A = repmat(Al(k,:),numS,1);
            Yobs = reshape(obslikZ(:,:,t),numS,S);
            py(k,t) = prod(sum(A.*Yobs,2));
        end
        py(:,t) = normalise(py(:,t));
    end
    
    
    
    
    
    
    
    
    
