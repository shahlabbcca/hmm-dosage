function [x,y,chrIndex] = makeDataVec(X,Y)
y = celldata2Vec(Y);
x = celldata2Vec(X);
chrIndex = ones(1,length(y));
index = 1;

for i = 1:24
    l = length(Y{i});
    I = index:(index+l-1);
    chrIndex(I) = i;
    index = index+l;
end