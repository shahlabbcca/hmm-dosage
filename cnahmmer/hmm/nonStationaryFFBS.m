function [samples,loglik] = nonStationaryFFBS(init_state_distrib,transmat,obslik,nsamples)

% forwards propagation, backwards sampling
%
% input
% init_state_distrib(q)
% transmat(q, q')
% obslik(q, t)
% nsamples
%
% output
% samples(t, s)  = sample s of discrete state at time  t


[Q T] = size(obslik);
scale = ones(1,T);

alpha = zeros(Q,T, 'single');
beta = zeros(Q,T,'single');
gamma = zeros(Q,T,'single');

t = 1;
alpha(:,1) = init_state_distrib(:) .* obslik(:,t);
[alpha(:,t), scale(t)] = normalize(alpha(:,t));
for t=2:T
    trans = reshape(transmat(:,:,t),[Q,Q]);
    m = trans' * alpha(:,t-1);
    alpha(:,t) = m(:) .* obslik(:,t);
    [alpha(:,t), scale(t)] = normalize(alpha(:,t));
    %assert(approxeq(sum(alpha(:,t)),1))
end
loglik = sum(log(scale));


%beta = zeros(Q,T);
t=T;
%beta(:,T) = ones(Q,1);
gamma(:,T) = alpha(:,T);
if nsamples > 0
    samples(t,:) = sample(gamma(:,T), nsamples);
end
for t=T-1:-1:1
    trans = reshape(transmat(:,:,t),[Q,Q]);
%     b = beta(:,t+1) .* obslik(:,t+1);
%     beta(:,t) = normalize(trans * b);
%     gamma(:,t) = normalize(alpha(:,t) .* beta(:,t));

    if nsamples > 0
        xi_filtered = (alpha(:,t) * obslik(:,t+1)') .* trans;
        for n=1:nsamples
            dist = xi_filtered(:,samples(t+1,n))+1e-8;
            samples(t,n) = sample(dist);
        end
    end
end

