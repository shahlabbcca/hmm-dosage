function [mu lambda pi rho loglik] = tmmml(Y,mu_0,lambda_0,nu,maxiter)
    % EM algorithm for Student-t mixture model - ML estimate
    %
    % function [mu lambda pi loglik] = TMM(Y,mu_0,lambda_0,nu,maxiter)
    %
    % Y          - data
    % mu_0       - initial value for Student-t means (1-by-K)
    % lambda_0   - initial values for Student-t precision (1-by-K)
    % maxiter    - maximum number of EM iterations
    % nu         - fixed values for Student-t dof (1-by-K)
    % N          - size of data
    % K          - number of discrete states
    
    
    K = length(mu_0);
    N = length(Y);
    py = zeros(K,N);
    mu = zeros(K,maxiter);
    lambda = zeros(K,maxiter);
    pi = ones(K,1)/K;
    loglik = zeros(1,maxiter);
    converged = 0;
    Nk = zeros(1,K); 
    
    rho = zeros(K,N); % responsibilities 
    
    mu(:,1) = mu_0;
    lambda(:,1) = lambda_0;
    % complete initialisation
    for k=1:K
        py(k,:)=tdistPDF(Y,mu_0(k),lambda_0(k),nu(k));
    end
    % initial label assignments
    rho = normalise(py,1);
    
    % initial weights
    pi = mean(rho,2);
    
    % now run EM    
    i = 1;
    loglik(i) = -Inf;
    while (~converged && (i < maxiter))
        i = i+1;
        disp(i);
        % E-step
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        rho = normalise(py.*(repmat(pi,[1,N])),1);
        % M-step
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        mu_i = reshape(mu(:,i-1),[1,K]);
        lambda_i = reshape(lambda(:,i-1),[1,K]);
        [mu(:,i),lambda(:,i), pi, u] = estimateTNoiseParamsML(Y,mu_i,lambda_i,nu,rho);    
        
        % re-calculate the likelihood
        for k=1:K
            py(k,:)=tdistPDF(Y,mu(k,i),lambda(k,i),nu(k));
        end
       
      
        % compute log-likelihood and check convergence
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        loglik(i) = sum(sum(log(py.*(repmat(pi,[1,N])))));
        if (loglik(i) <= loglik(i-1))
            converged = 1;
        end   
    end
    if converged 
        i = i-1;
    end    
    mu = mu(:,1:i);
    lambda = lambda(:,1:i);
    loglik = loglik(1:i);
    
    