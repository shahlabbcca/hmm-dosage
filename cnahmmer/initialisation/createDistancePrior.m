function rho = createDistancePrior(X,chrIndex,L)
    rho = zeros(1,length(X));
    chrs = unique(chrIndex);
    for c = 1:length(chrs)
        I = find(chrIndex == chrs(c));
        x = [0, X(I)'];
        d = x(2:end)-x(1:(end-1));
        rho(I) = distancePrior(d,L);
    end
    