function [mu_0,lambda_0,eta,m,gamma,S] = initialiseTHyperparams(y,f,K)
    %disp([f,K]);
    stdy = nanstd(y);
    mu_0 = [-stdy, 0, stdy]*f;
    lambda_0 = [1000 1000 1000]*stdy;
    
    eta = ones(1,3)*2000;
    m = [-stdy, 0, stdy]*(f);
    gamma = ones(1,3);
    S = [0.0001 0.0001 0.0001];
 %   S = ([stdy stdy stdy])/sqrt(K);
 %   S = [0.001 0.001 0.001];