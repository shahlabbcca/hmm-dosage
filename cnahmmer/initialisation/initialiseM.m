function M = initialiseM(Z)
% function M = initialiseM(Z)
% takes the Zs assigned to a given cluster and creates the discrete profile
[numP,N] = size(Z);
M = zeros(1,N);
% 
% I = find(sum(Z==1)==numP);
% M(I) = 2;
% I = find(sum(Z==3)==numP);
% M(I) = 3;
% 
Mtemp = mode(Z);
M(find(Mtemp==2)) = 1;
M(find(Mtemp==1)) = 2;
M(find(Mtemp==3)) = 3;



