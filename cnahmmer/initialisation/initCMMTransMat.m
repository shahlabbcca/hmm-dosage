function A = initCMMTransMat(d,S)
    A = zeros(S,S,length(d));
    for j = 1:S
        for k = 1:S
            if (j==k)
                A(j,k,:) = 1-d;
            else
                A(j,k,:) = d/(S-1);
            end
        end
    end
    
        