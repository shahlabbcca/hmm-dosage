function [probM] = initMasterSequence(Z,S)
    % function initMasterSequence
    % takes a numS-by-N vector of state assigments where Z(s,t)=k is the 
    % state (k \in 1:S) at position t in sample s
    % Returns a S-by-N matrix denoting the normalised counts of
    % Z(s,t) = k
    [numS, N] = size(Z);
    counts = zeros(S,N);
    
    for k=1:S
        for s=1:numS
            inds = find(Z(s,:)==k);
            counts(k,inds) = counts(k,inds)+1;
        end
    end
    probM = normalize(counts,1);
    
    
    
    
       