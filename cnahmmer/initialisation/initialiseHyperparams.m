function [mu_0,var_0,a_0,b_0] = initialiseHyperparams(y,f,K)
    neut = 2;
    if K>4
        neut = 3;
    end
    stdy = nanstd(y);
    var_0 = ones(1,K)*0.0001;
    mu_0 = zeros(1,K);
    a_0 = ones(1,K)*(10*stdy);
    b_0 = ones(1,K);
    if K==3
       mu_0(1) = -f*stdy;
       mu_0(3) = f*stdy;
    end
    if K==4
        mu_0(1) = -f*stdy/2;
        mu_0(3) = f*stdy;
        mu_0(4) = 2*f*stdy;
    end 
    if K==5
        mu_0(1) = -f*stdy;
        mu_0(2) = -(f/2)*stdy;
        mu_0(4) = f*stdy;
        mu_0(5) = 2*f*stdy;
    end 
    if K==6
        mu_0(1) = -f*stdy;
        mu_0(2) = -(f/2)*stdy;
        mu_0(4) = f*stdy;
        mu_0(5) = 2*f*stdy;
        mu_0(6) = 3*f*stdy;
    end 
    