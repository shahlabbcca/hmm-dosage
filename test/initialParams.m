%initial mean
mu_som = log2(([0.5,1,2,3,5,7])/2);
%prior precision
lambda_som = [20,20,200,20,20,20];

% degrees of freedom for Student-t emission density - fixed
%increasing this results in approximating the Gaussian
nu_som = [2.1,2.1,2.1,2.1,2.1,2.1];

%stationary distribution for class assignment
kappa_som = [0.05,0.05,0.65,0.05,0.05,0.05]*1000;
    

%prior precision of m (reduce for more 'elasticity' in model fitting)
eta_som = [50000, 50000, 500000, 50000, 50000, 50000];

%hyperparameters for lambda
%gamma = ones(1,5)+2;
%K=length(eta);
%S = ones(1,K)*((nanstd(y)/sqrt(K))^2);

% boolean flag to include or exclude chrX in parameter estimation
includeXinParamEstimate = true;

% maximum iterations for EM convergence (20-50 should be sufficient)
maxiter = 50;

%stationary values for txn matrix
strength = 10000;
e = 0.9999;

%CNV loss param setup
mu_cnvL = log2(([0.5,1])/2);
lambda_cnvL = [20,20];
nu_cnvL = [2.1,2.1];
kappa_cnvL = [0.05,0.05]*1000;
eta_cnvL = [50000, 50000];

%CNV gain param setup
mu_cnvG = log2(([3,5,7])/2);
lambda_cnvG = [20,20,20];
nu_cnvG = [2.1,2.1,2.1];
kappa_cnvG = [0.05,0.05,0.05]*1000;
eta_cnvG = [50000, 50000, 50000];

% combine all the parameters (som + cnv)
% prior mean on the means (default to initial values)
mu_0 = cat(2,mu_som,mu_cnvL,mu_cnvG);
m= mu_0;
lambda_0 = cat(2,lambda_som,lambda_cnvL,lambda_cnvG);
nu = cat(2,nu_som,nu_cnvL,nu_cnvG);
kappa = cat(2,kappa_som,kappa_cnvL,kappa_cnvG);
eta = cat(2,eta_som,eta_cnvL,eta_cnvG);

performLogisticTransform = 0;
% alpha parameter for logit transformation of CNV frequencies
filterLevel(1) = 0.60;
filterLevel(2) = 0.97;
filterLevel(3) = 0.99;
filterLevel(4) = 0.97;
filterLevel(5) = 0.00;

% multiply CNV frequency to obtain Dirichlet counts 
cnvStrength = 10000;
nonCNVStrength = 100;

zeroFreqBool = 1;

save initialParams.mat
