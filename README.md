## Running the compiled software ##
There are 2 ways to run the compiled software: 1) executable or 2) shell script. These options are offered by the Matlab as a result of using the MCR compiler. If you have MCR already installed and added to your path (specifically the LD_LIBRARY_PATH environment variable) then you can use the executable; otherwise, use the shell script as it allows you to manually specify the MCR install path. In both cases, the same input/output files and parameters are required:

```
hmmK11LogR(infile,freqfile,paramSetFile,outfile,segoutfile,paramfile,chr)

infile         Input file with format in 3-columns tab-delimited:
                 1) chr 2) position 3) raw copy number (not in log2 scale),
                 where 2 is baseline neutral. The program will transform
                 the values into log2 scale as follows: log2(y/2).
                 There are N rows, one for each probe.

freqfile       Frequency file for probe-level CNV prior.
                 9-column tab-delimited file:
                 1) id (can be arbitrary)
                 2) SNP array probe id (can be arbitrary)
                 3) chr
                 4) position
                 5) CNV homozygous deletion frequency
                 6) CNV hemizygous deletion frequency
                 7) CNV low-level amplification frequency
                 8 ) CNV med-level amplification frequency
                 9) CNV high-level amplification

paramSetFile   Parameter intialization file is a matlab binary (.mat) file.
                 This file contains model and setting paramters necessary to
                 run the program.  See example in manual for details.

outfile        Output file for probe-level results.
                 5-column tab-delimited file:
                 1) chr 2) start 3) stop 4) logR
                 5) HMM state prediction where
                  1 homozygous deletion, 2 hemizygous deletion, 3 neutral,
                  4 low-level amplification (gain),
                  5 medium-level amplification,
                  6 high-level amplification,
                  7 homozygous deletion CNV,
                  8 hemizygous deletion CNV,
                  9 low-level amplification CNV,
                  10 medium-level amplification CNV,
                  11 high-level amplification CNV.
                  States 1,2,4,5,6 are somatic CNA events.
                  States 7-11 are germline CNV events.

segoutfile     Output file for segmentation results.
                 5-column tab-delimited file:
                 1) chr 2) start 3) stop
                 4) HMM state prediction (same as for outfile) 5) logR

paramfile      Output file for converged parameters after model training
                 using Expectation Maximization (EM) algorithm.
                 Means and precisions for each HMM class/state are saved for
                 iteration of EM.  This can be useful for comparing initial
                 and converged values as well as checking for label switching.
                 The output file is a Matlab binary (.mat).

chr            Integer denoting a chromosome.
                 If chr is a value between [1 to 24], then all raw
                 copy number data for N probes will be normalized by
                 the median of the probes in chr.  If chr=0, then
                 data is normalized using median of all chromosomes.
                 If chr=-1, data is normalized using the default
                 neutral value (e.g. 2)
```

## AN EXAMPLE ##

Here is an example of how to use the shell script. Refer to runTest_script.sh to see how each input file is used. Also, notice the formatting of each input and output file in `<$install_dir>/HMMK11_0.1.0/test/`
```shell
cd <$install_dir>/HMMK11_0.1.0/test/
 ./runTest.sh <$mcr_dir>/V77/
```

## Running the software in Matlab ##
If you have a Matlab installed and wish to run within the Matlab environment, then you can start up Matlab and add the source files before executing the main function.
```matlab
>> addpath(genpath("<$install_dir>/HMMK11_0.1.0/cnahmmer"))
>> addpath(genpath("<$install_dir>/HMMK11_0.1.0/stats"))
>> % assuming your have all the parameters to the main function assigned...
>> hmmK11LogR(infile,freqfile,paramSetFile,outfile,segoutfile,paramfile,chr)
```