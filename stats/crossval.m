function loss = crossval(fun,varargin)
%CROSSVAL Loss estimate using cross-validation
%   LOSS = CROSSVAL(FUN,X) computes 10-fold cross-validation for the
%   function FUN, applied to the data in X.  FUN is a function handle that
%   is called 10 times with two inputs, the training set of X and the test
%   set of X, as follows:
%
%      TESTLOSS = FUN(XTRAIN,XTEST)
%
%   FUN returns TESTLOSS, the loss value computed on the test subset using
%   the model learned from the training set of X. X can be a column vector
%   or a matrix.  Rows of X correspond to observations; columns correspond
%   to features. Each row of LOSS contains the loss value for one test set.
%   If TESTLOSS is a matrix or array, then it is converted to a row vector
%   in the linear indexing order for storage in a row of LOSS. Typical LOSS
%   measures include the mean squared error for regression, or
%   misclassification proportions for classification.
%
%   LOSS = CROSSVAL(FUN,X,Y,...) is used when the data are stored in
%   separate variables X, Y, ....  All variables (column vectors, matrices
%   or arrays) must have the same number of rows.  FUN is called with the
%   training subsets of X, Y, ..., followed by the test subsets of X, Y,
%   ..., as follows:
%
%      TESTLOSS = FUN(XTRAIN,YTRAIN,...,XTEST,YTEST,...)
%
%   LOSS = crossval(...,'PARAM1',val1,'PARAM2',val2,...) specifies
%   optional parameter name/value pairs chosen from the following:
%
%   'Kfold'        The number of folds K for K-fold cross-validation.
%   'Holdout'      The ratio or the number of observations P for holdout. P
%                  must be a scalar. When 0<P<1, it randomly selects
%                  approximately P*N observations for the test set. When P is
%                  an integer, it randomly selects P observations for the
%                  test set.
%   'Leaveout'     The value must be 1. Leave-one-out cross-validation is
%                  used.
%   'Partition'    A CVPARTITION object C.
%   'Stratify'     A column vector G indicating the group information for
%                  stratification. Both training and test sets have roughly
%                  the same class proportions as in G.  CROSSVAL treats
%                  NaNs or empty strings in GROUP as missing values and the
%                  corresponding rows of (X,Y,...) are ignored.
%   'Mcreps'       A positive integer indicating the number of Monte-Carlo
%                  repetitions for validation. The values of LOSS from all
%                  the Monte-Carlo repetitions are concatenated along the
%                  first dimension.
%
%
%   Only one of 'Kfold', 'Holdout','Leaveout' or 'Partition' parameters can
%   be specified.  10-fold cross-validation is the default if none of the
%   'Kfold', 'Holdout','Leaveout' or 'Partition' parameters is provided.
%   The 'Partition' parameter cannot be specified with 'Stratify'. If the
%   values of both 'Partition' and 'Mcreps' are provided, the first
%   Monte-Carlo repetition will use the partition information contained in
%   the given CVPARTITION object, the REPARTITION method will be called on
%   this CVPARTITION object to generate a new partition for each of the
%   remaining Monte-Carlo repetitions.
%
%   Examples: 
%      % Compute mean square error for regression using 10-fold
%      % cross-validation
%      load('fisheriris');
%      y = meas(:,1);
%      x = [ones(size(y,1),1) meas(:,2:4)];
%      sse = crossval(@regf,x,y);
%      cvMse = sum(sse)/length(y);
%
%      where REGF is a MATLAB function such as:
%      function sse = regf(xtrain,ytrain,xtest,ytest)
%         yfit = xtest * regress(ytrain,xtrain);
%         sse = norm(ytest-yfit).^2;
%
%
%      % Compute mis-classification rate using stratified 10-fold
%      % cross-validation
%      load('fisheriris');
%      y = species;
%      cp = cvpartition(y,'k',10);
%      e = sum(crossval(@classf,meas,y,'partition',cp))/sum(cp.TestSize);
%
%      where CLASSF is a MATLAB function such as:
%      function err = classf(xtrain,ytrain,xtest,ytest)
%        yfit = classify(xtest,xtrain,ytrain);
%        err = sum(~strcmp(ytest,yfit));
%     
%   See also CVPARTITION, CVPARTITION/REPARTITION.

%   References:
%   [1] Hastie, T. Tibshirani, R, and Friedman, J. (2001) The Elements of
%       Statistical Learning, Springer, pp. 214-216.

% Copyright 2007 The MathWorks, Inc.
% $Revision: 1.1.6.2 $  $Date: 2007/12/10 23:05:21 $

if nargin < 2 || isempty(varargin)
    error('stats:crossval:TooFewInputs',...
        'At least two inputs are needed.');
end

if ~isa(fun,'function_handle')
    error('stats:crossval:BadFun',...
        'FUN must be a function handle.');
end

n = size(varargin{1},1);
if n <= 1
    error('stats:crossval:TooFewDataRows',...
        'The data arguments X must have at least two rows');
end

nData = length(varargin);
for i = 2:length(varargin);
    if size(varargin{i},1) ~= n
        if ~(ischar(varargin{i}) && size(varargin{i},1) ==1 )
            error('stats:crossval:MismatchedDataRows',...
                'Data arguments X,Y,... must have the same number of rows.');
        else
            nData = i-1;
            break;
        end
    end
end
data = varargin(1:nData);
varargin(1:nData)= [];

pnames = {'kfold'  'holdout'  'leaveout' 'mcreps' 'stratify' 'partition'};
dflts =  {[]        []          []        1          []         []  };
[eid,errmsg,nfolds,holdout, leaveout, mcreps, stratify, cvp] ...
    = statgetargs(pnames, dflts, varargin{:});
if ~isempty(eid)
    error(sprintf('stats:crossval:%s',eid),errmsg);
end

if ~isempty(leaveout) && leaveout ~= 1
    error('stats:crossval:UnsupportedLeaveout',...
        '''Leaveout'' must be 1. Only leave-one-out is supported.');
end

if ~( isnumeric(mcreps) && isscalar(mcreps) && mcreps == round(mcreps)...
        && mcreps >= 1)
    error('stats:crossval:BadMcreps',...
        '''Mcreps'' must be a positive integer.');
end

choices=[];

cvopts = sum( [~isempty(holdout), ~isempty(nfolds) , ~isempty(leaveout)]);
if cvopts > 1
    error('stats:crossval:InconsistentOpts',...
        'Specify only one of the following: ''Kfold'',''Holdout'' and ''Leaveout''.');
elseif ~isempty(cvp)
    if cvopts >0 || ~isempty(stratify)
        error('stats:crossval:InconsistentOpts',...
            ['''Partition'' cannot be used with ''Kfold'',''Holdout'' ',...
            '''Leaveout'' or ''Stratify''.']);
    elseif ~isa(cvp,'cvpartition')
        error('stats:crossval:Badcvp',...
          '''Partition'' must be a CVPARTITION object.');
    elseif cvp.N ~= n
        error('stats:crossval:MismatchedDataRows',...
            'The ''N'' property of ''PARTITION'' must equal the number of rows in X.');
    end
    choices = 'cvpartition';
    if mcreps > 1 && (strcmp(cvp.Type,'resubstitution') ...
                     || strcmp(cvp.Type,'leaveout'))
            mcreps = 1;
            warning('stats:crossval:InvalidMcreps',...
                '''Mcreps'' is set to 1 for Leave-one-out cross-validation or Resubstitution.');
    end
else
    if ~isempty(stratify)
        stratify = grp2idx(stratify);
        if size( stratify,1) ~= n
            error('stats:crossval:MismatchedDataRows',...
                '''Stratify'' must have the same number of rows as X.');
        end

    end
    if cvopts == 0
        choices = 'kfold';
        cvarg = 10; %default cross-validation option
    elseif ~isempty(nfolds)
        choices = 'kfold';
        cvarg = nfolds;
    elseif ~isempty(holdout)
        choices ='holdout';
        cvarg = holdout;
    elseif ~isempty(leaveout)
        if mcreps > 1
            mcreps = 1;
            warning('stats:crossval:InvalidMcreps',...
                'The value of ''Mcreps'' is set to 1 for Leave-one-out cross-validation.');
        end
        choices = 'leaveout';
        cvarg = 1;
    end
end

if ~strcmp(choices,'cvpartition')
    if isempty(stratify)
        cvp = cvpartition(n, choices,cvarg);
    else
        cvp = cvpartition(stratify, choices,cvarg);
    end
end

nData = size(data,2);
arg = cell(2*nData,1);
iter=0;

for mr = 1:mcreps

    if mr > 1
        cvp = cvp.repartition();
    end

    for i = 1:cvp.NumTestSets
        train = cvp.training(i);
        test = cvp.test(i);
        % Take subsets of the inputs
        for k = 1:nData
            arg{k} = data{k}(train,:);
            arg{nData+k} = data{k}(test,:);
        end

        % Apply the function to the current subset
        try
            funResult = fun(arg{:});
        catch
            [errMsg,errID] = lasterr;
            if strcmp('MATLAB:UndefinedFunction', errID) ...
                    && ~isempty(strfind(errMsg, func2str(fun)))
                error('stats:crossval:FunNotFound',...
                    'The function ''%s'' was not found.', func2str(fun));
            else
                error('stats:crossval:FunError',...
                    'The function ''%s'' generated the following error:\n%s',...
                    func2str(fun),lasterr);
            end
        end

        % Accumulate the results
        iter=iter+1;
        if mr==1 && i == 1
            szFunResult=size(funResult);
            loss=funResult(:)';
            loss(cvp.NumTestSets * mcreps,1:numel(loss))=loss;
        else
            if ~isequal(size(funResult),szFunResult)
                error('stats:crossval:FunOutSizeMismatched',...
                    'The size of outputs of function ''%s'' should be same.', func2str(fun));
            end
            loss( iter,:)=funResult(:)';
        end
    end
end
end




