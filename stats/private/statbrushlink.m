function statbrushlink(handles,tf)
%STATNOBRUSHLINK Set brushing and linking for graphics objects
%   STATNOBRUSHLINK(H,TF) turns off brushing and linking for the objects
%   whose handles are given in the array H if TF is false, and turns it on
%   if TF is true.

%   $Revision: 1.1.6.1 $  $Date: 2007/12/10 23:08:18 $
%   Copyright 2007 The MathWorks, Inc.

for j=1:numel(handles)
    gObj = handles(j);
    if ishandle(gObj)
        brushBehavior = hggetbehavior(gObj,'Brush');
        brushBehavior.Enable = tf;
        linkBehavior = hggetbehavior(gObj,'Linked');
        linkBehavior.Enable = tf;
    end
end

