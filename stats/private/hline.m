function h=hline(varargin)
%HLINE draws horizontal lines
%HLINE(X) draws one horizontal line for each element in vector x
%HLINE(AX,X) draws the lines to the axes specified in AX
%HLINE(X,...) accepts HG param/value pairs for line objects
%H=HLINE(...) returns a handle to each line created

if isempty(varargin) || (ishandle(varargin{1}) && length(varargin)==1)
    error('stats:hline:NotEnoughArgs','Not enough arguments');
end

if ishandle(varargin{1})
    ax=varargin{1};
    varargin=varargin(2:end);
else
    ax=gca;
end

x = varargin{1};
varargin=varargin(2:end);
    
hh=graph2d.constantline(x,'parent',ax,varargin{:});

if nargout>0
    h=hh;
end