function h=vline(varargin)
%VLINE draws horizontal lines
%VLINE(X) draws one horizontal line for each element in vector X
%VLINE(AX,X) draws the lines to the axes specified in AX
%VLINE(X,...) accepts HG param/value pairs for line objects
%H=VLINE(...) returns a handle to each line created

if isempty(varargin) || (ishandle(varargin{1}) && length(varargin)==1)
    error('stats:hline:NotEnoughArgs','Not enough arguments');
end

if ishandle(varargin{1})
    ax=varargin{1};
    varargin=varargin(2:end);
else
    ax=gca;
end

x = varargin{1};
varargin=varargin(2:end);
    
hh=graph2d.constantline(x,'parent',ax,varargin{:});
hh = changedependvar(hh,'x');

if nargout>0
    h=hh;
end