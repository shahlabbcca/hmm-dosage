function [y,chrIndex,start,rho,Z] = hmmK11LogR(infile,freqfile,paramSetFile,outfile,segoutfile,paramfile,chr)
% FUNCTION hmmK11LogR(infile,freqfile,paramSetFile,outfile,segoutfile,paramfile,chr)
%
% Predict somatic copy number alterations and germline copy number variations from a single SNP genotyping array
%
% author: Sohrab Shah <sshah@bccrc.ca>
%         Gavin Ha <gha@bccrc.ca>
%         Dept of Molecular Oncolgy/Centre for Translational and Applied Genomics
%         British Columbia Cancer Agency
% date  : December 6, 2010
%
% infile         Input file with format in 3-columns tab-delimited: 1) chr 2) position 3) raw copy number (not in log2 scale), where 2 is baseline neutral.
%                The program will transform the values into log2 scale as follows: log2(y/2).  There are N rows, one for each probe.
% freqfile       Frequency file for probe-level CNV prior.  9-column tab-delimited file: 1) id (can be arbitrary) 2) SNP array probe id (can be arbitrary) 
%                3) chr 4) position 5) CNV homozygous deletion frequency 6) CNV hemizygous deletion frequency 7) CNV low-level amplification 
%                frequency 8) CNV med-level amplification frequency 9) CNV high-level amplification
% paramSetFile   Parameter intialization file is a matlab binary (.mat) file.  This file contains model and setting paramters necessary to run 
%                the program.  See example in manual for details.
% outfile        Output file for probe-level results. 5-column tab-delimited file: 1) chr 2) start 3) stop 4) logR 4) HMM state prediction  
%                where <hmmstate>=1 homozygous deletion, 2 hemizygous deletion, 3 neutral, 4 means low-level
%                amplification, 5 means med-level amplification, 6 means high-level
%                amplification.  Output file is automatically appended with "_cna.txt"
% segoutfile     Output file for segmentation results. 5-column tab-delimited file: 1) chr 2) start 3) stop 4) HMM state prediction (same as for outfile) 5) logR
%                Output file is automatically appended with "_segs.txt"
% paramfile      Output file for converged parameters after model training using Expectation Maximization (EM) algorithm.  Means and precisions 
%                for each HMM class/state are saved for iteration of EM.  This can be useful for comparing initial and converged values as well as 
%                checking for label switching.  The file is a Matlab binary (.mat).
% chr            Integer denoting a chromosome.  If chr is a value between [1 to 24], then all raw copy number data for N probes will be normalized
%                by the median of the probes in chr.  If chr=0, then data is normalized using median of all chromosomes.  If chr=-1, data is normalized 
%                using the default neutral value (e.g. 2)


chr = str2num(chr);


%==========================================================================
% SETTING UP THE HMM PARAMETERS
%==========================================================================

% maximum iterations for EM convergence (20-50 should be sufficient)
%maxiter = 20;
disp('Running HMMK11'); 
disp(['hmmK11LogR: Loading raw copy number data ',infile]);
data     = load(infile);
chrIndex = data(:,1);
start    = data(:,2);
y        = data(:,3);
y(y==0)  = NaN;

% defaults if not included in param file
zeroFreqBool = 0; % default if zero frequency, use residuals
%load param settings file
load(paramSetFile);

%include/exclude chrX
if includeXinParamEstimate
    cI = chrIndex<=23;
else
    cI = chrIndex<23;
end

%subtract median
if chr > 0
    y = real(log2(y/nanmedian(y(chrIndex==chr))));
elseif chr == 0
    y = real(log2(y/nanmedian(y(cI))));
else
    y = real(log2(y/2));
end
% read in CNV frequency file
disp('hmmK11LogR: Reading CNV frequency file.');
priorFreqs = importdata(freqfile);
F = zeros(size(priorFreqs.data(:,3:end)));

if performLogisticTransform
% perform logistic transformation
    for i=1:(length(mu_cnvL)+length(mu_cnvG)) %total number of CNV states
       F(:,i) = logisticTransformCNVPrior(priorFreqs.data(:,i+2),filterLevel(i),0);
    end
else   
% do not perform logistic transformation
    F = priorFreqs.data(:,3:end);
end
% transform our CNV frequencies into Dirichlet counts
%F = F * cnvStrength;
% done in C code

K=length(mu_0);
gamma = ones(1,K)+2;
S = ones(1,K)*((nanstd(y)/sqrt(K))^2);
[mus lambdas pi loglik Z rho,segs] = THMMViterbiK11(y',chrIndex,cI,mu_0,lambda_0,nu,eta,m,gamma,S,kappa,maxiter, F,cnvStrength,length(mu_cnvL),length(mu_cnvG),zeroFreqBool,nonCNVStrength,strength,e);
mus
lambdas
loglik


save(paramfile,'mus','lambdas','loglik');
%==========================================================================
% WRITE OUT RESULTS
%==========================================================================
% write out all segments
numsegs=0;
for i=1:23
    [ms,ns] = size(segs{i});
    numsegs = numsegs + ms;
end


flatsegs = zeros(numsegs,5);
index=1;
for i=1:23
    [ms,ns] = size(segs{i});
    I = index:(index+ms-1);
    cI = find(chrIndex==i);

    flatsegs(I,1) = i;
    flatsegs(I,2) = start(cI(segs{i}(:,1)));
    flatsegs(I,3) = start(cI(segs{i}(:,2)));
    flatsegs(I,4) = segs{i}(:,3);
    for j=1:length(I)

        flatsegs(I(j),5) = nanmedian(y((cI(segs{i}(j,1))):(cI(segs{i}(j,2)))));
    end

    index = index+ms;
end

% write out the segments
fid = fopen(segoutfile,'wt');
for i = 1:numsegs
    fprintf(fid,'%d\t%d\t%d\t%d\t%1.4f\n',flatsegs(i,:));
end
fclose(fid);

% now write out all data with copy number calls
fullmatrix = [chrIndex,start,start,y,Z];
fid = fopen(outfile,'wt');
for i=1:length(y)
    fprintf(fid,'%d\t%d\t%d\t%1.4f\t%d\n',fullmatrix(i,:));
end
fclose(fid);

