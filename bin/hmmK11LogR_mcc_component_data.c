/*
 * MATLAB Compiler: 4.7 (R2007b)
 * Date: Thu Aug 25 02:19:13 2011
 * Arguments: "-B" "macro_default" "-m" "-W" "main" "-T" "link:exe"
 * "hmmK11LogR.m" 
 */

#include "mclmcr.h"

#ifdef __cplusplus
extern "C" {
#endif
const unsigned char __MCC_hmmK11LogR_session_key[] = {
    'A', '6', '8', 'D', '5', 'E', 'C', '2', 'B', '6', '1', '8', 'B', '0', 'B',
    'A', 'A', 'F', '4', '6', '6', '3', 'D', 'D', '0', '5', 'A', 'C', '1', '2',
    '8', 'E', 'E', '2', 'D', '0', '7', 'A', 'A', '7', '8', 'D', '5', '3', 'D',
    '5', '1', 'C', 'A', '2', '3', '4', '7', '6', '6', '9', 'A', '2', 'B', '9',
    '7', '8', '2', '2', 'D', '6', 'C', 'B', '8', 'D', '3', 'E', '8', '9', '4',
    '6', 'E', '5', 'F', '1', 'D', '7', '1', '8', 'F', '9', 'E', '2', 'B', '4',
    '3', '2', 'B', '2', 'C', '3', 'C', 'A', '3', '4', 'E', '8', '1', '1', '2',
    '2', 'E', '8', 'F', '0', 'A', '1', '8', '1', 'E', '7', '7', '2', 'D', 'A',
    '1', '2', 'B', '8', 'C', 'F', '8', 'B', '8', 'F', '1', '1', '4', 'A', 'D',
    'F', 'E', '1', '4', '8', '5', '1', 'E', '7', '3', 'F', 'C', '8', '3', '4',
    '5', '4', '6', 'C', '5', '5', '1', 'C', 'F', '5', 'D', '9', '4', 'F', 'C',
    '2', '2', '8', '6', 'D', '9', '9', 'D', 'B', 'A', 'C', '9', 'A', '5', '0',
    'A', 'C', '8', 'C', 'C', 'B', '4', '8', '5', '1', 'D', 'D', 'B', 'F', 'C',
    '3', '8', '5', '0', '1', '0', '1', 'E', '0', '3', '4', '0', '7', 'C', '7',
    'D', '1', 'C', '5', '1', 'C', '4', 'F', '9', 'E', 'F', '4', 'F', 'C', '0',
    'C', '7', '5', '7', 'F', 'D', '2', '5', 'B', '2', 'D', '9', 'C', '3', '1',
    '9', '0', '6', '0', 'E', '1', '8', '6', 'C', '0', 'C', 'F', '6', '9', '9',
    'A', '\0'};

const unsigned char __MCC_hmmK11LogR_public_key[] = {
    '3', '0', '8', '1', '9', 'D', '3', '0', '0', 'D', '0', '6', '0', '9', '2',
    'A', '8', '6', '4', '8', '8', '6', 'F', '7', '0', 'D', '0', '1', '0', '1',
    '0', '1', '0', '5', '0', '0', '0', '3', '8', '1', '8', 'B', '0', '0', '3',
    '0', '8', '1', '8', '7', '0', '2', '8', '1', '8', '1', '0', '0', 'C', '4',
    '9', 'C', 'A', 'C', '3', '4', 'E', 'D', '1', '3', 'A', '5', '2', '0', '6',
    '5', '8', 'F', '6', 'F', '8', 'E', '0', '1', '3', '8', 'C', '4', '3', '1',
    '5', 'B', '4', '3', '1', '5', '2', '7', '7', 'E', 'D', '3', 'F', '7', 'D',
    'A', 'E', '5', '3', '0', '9', '9', 'D', 'B', '0', '8', 'E', 'E', '5', '8',
    '9', 'F', '8', '0', '4', 'D', '4', 'B', '9', '8', '1', '3', '2', '6', 'A',
    '5', '2', 'C', 'C', 'E', '4', '3', '8', '2', 'E', '9', 'F', '2', 'B', '4',
    'D', '0', '8', '5', 'E', 'B', '9', '5', '0', 'C', '7', 'A', 'B', '1', '2',
    'E', 'D', 'E', '2', 'D', '4', '1', '2', '9', '7', '8', '2', '0', 'E', '6',
    '3', '7', '7', 'A', '5', 'F', 'E', 'B', '5', '6', '8', '9', 'D', '4', 'E',
    '6', '0', '3', '2', 'F', '6', '0', 'C', '4', '3', '0', '7', '4', 'A', '0',
    '4', 'C', '2', '6', 'A', 'B', '7', '2', 'F', '5', '4', 'B', '5', '1', 'B',
    'B', '4', '6', '0', '5', '7', '8', '7', '8', '5', 'B', '1', '9', '9', '0',
    '1', '4', '3', '1', '4', 'A', '6', '5', 'F', '0', '9', '0', 'B', '6', '1',
    'F', 'C', '2', '0', '1', '6', '9', '4', '5', '3', 'B', '5', '8', 'F', 'C',
    '8', 'B', 'A', '4', '3', 'E', '6', '7', '7', '6', 'E', 'B', '7', 'E', 'C',
    'D', '3', '1', '7', '8', 'B', '5', '6', 'A', 'B', '0', 'F', 'A', '0', '6',
    'D', 'D', '6', '4', '9', '6', '7', 'C', 'B', '1', '4', '9', 'E', '5', '0',
    '2', '0', '1', '1', '1', '\0'};

static const char * MCC_hmmK11LogR_matlabpath_data[] = 
  { "hmmK11LogR/", "toolbox/compiler/deploy/",
    "share/data7/gha/HMMK11_0.1.0/stats/",
    "share/data7/gha/HMMK11_0.1.0/cnahmmer/GaussHMM/",
    "share/data7/gha/HMMK11_0.1.0/cnahmmer/KPMtools/",
    "share/data7/gha/HMMK11_0.1.0/cnahmmer/hmm/",
    "share/data7/gha/HMMK11_0.1.0/cnahmmer/parameter_estimation/",
    "share/data7/gha/HMMK11_0.1.0/cnahmmer/util/",
    "$TOOLBOXMATLABDIR/general/", "$TOOLBOXMATLABDIR/ops/",
    "$TOOLBOXMATLABDIR/lang/", "$TOOLBOXMATLABDIR/elmat/",
    "$TOOLBOXMATLABDIR/elfun/", "$TOOLBOXMATLABDIR/specfun/",
    "$TOOLBOXMATLABDIR/matfun/", "$TOOLBOXMATLABDIR/datafun/",
    "$TOOLBOXMATLABDIR/polyfun/", "$TOOLBOXMATLABDIR/funfun/",
    "$TOOLBOXMATLABDIR/sparfun/", "$TOOLBOXMATLABDIR/scribe/",
    "$TOOLBOXMATLABDIR/graph2d/", "$TOOLBOXMATLABDIR/graph3d/",
    "$TOOLBOXMATLABDIR/specgraph/", "$TOOLBOXMATLABDIR/graphics/",
    "$TOOLBOXMATLABDIR/uitools/", "$TOOLBOXMATLABDIR/strfun/",
    "$TOOLBOXMATLABDIR/imagesci/", "$TOOLBOXMATLABDIR/iofun/",
    "$TOOLBOXMATLABDIR/audiovideo/", "$TOOLBOXMATLABDIR/timefun/",
    "$TOOLBOXMATLABDIR/datatypes/", "$TOOLBOXMATLABDIR/verctrl/",
    "$TOOLBOXMATLABDIR/codetools/", "$TOOLBOXMATLABDIR/helptools/",
    "$TOOLBOXMATLABDIR/demos/", "$TOOLBOXMATLABDIR/timeseries/",
    "$TOOLBOXMATLABDIR/hds/", "$TOOLBOXMATLABDIR/guide/",
    "$TOOLBOXMATLABDIR/plottools/", "toolbox/local/" };

static const char * MCC_hmmK11LogR_classpath_data[] = 
  { "" };

static const char * MCC_hmmK11LogR_libpath_data[] = 
  { "" };

static const char * MCC_hmmK11LogR_app_opts_data[] = 
  { "" };

static const char * MCC_hmmK11LogR_run_opts_data[] = 
  { "" };

static const char * MCC_hmmK11LogR_warning_state_data[] = 
  { "off:MATLAB:dispatcher:nameConflict" };


mclComponentData __MCC_hmmK11LogR_component_data = { 

  /* Public key data */
  __MCC_hmmK11LogR_public_key,

  /* Component name */
  "hmmK11LogR",

  /* Component Root */
  "",

  /* Application key data */
  __MCC_hmmK11LogR_session_key,

  /* Component's MATLAB Path */
  MCC_hmmK11LogR_matlabpath_data,

  /* Number of directories in the MATLAB Path */
  40,

  /* Component's Java class path */
  MCC_hmmK11LogR_classpath_data,
  /* Number of directories in the Java class path */
  0,

  /* Component's load library path (for extra shared libraries) */
  MCC_hmmK11LogR_libpath_data,
  /* Number of directories in the load library path */
  0,

  /* MCR instance-specific runtime options */
  MCC_hmmK11LogR_app_opts_data,
  /* Number of MCR instance-specific runtime options */
  0,

  /* MCR global runtime options */
  MCC_hmmK11LogR_run_opts_data,
  /* Number of MCR global runtime options */
  0,
  
  /* Component preferences directory */
  "hmmK11LogR_3FB10824B7049FA5FB7CEECD72457D05",

  /* MCR warning status data */
  MCC_hmmK11LogR_warning_state_data,
  /* Number of MCR warning status modifiers */
  1,

  /* Path to component - evaluated at runtime */
  NULL

};

#ifdef __cplusplus
}
#endif


